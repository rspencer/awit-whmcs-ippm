<?php
/*
 * AWIT IPPM - Common functions
 * Copyright (c) 2013-2014, AllWorldIT
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */



// Load config
require_once('awit_ippm_config.php');



/*
 * Globals setup by awit_ippm_init:
 *
 * AWIT_IPPM_IVER
 * AWIT_IPPM_ADMIN_USER
 * AWIT_IPPM_SUPPORTED_FIELDS
 * AWIT_IPPM_CONFIG_CUSTOM_FIELDS
 * AWIT_IPPM_CONFIG_CUSTOM_FIELDS_REVERSE
 *
 */

// Admin user of biling module
$GLOBALS['AWIT_IPPM_ADMIN_USER'] = 'awit_ippm';
// Internal version
$GLOBALS['AWIT_IPPM_IVER'] = '201404201209';



/**
 * Log debugging information
 * Set a level from 1 to 5, 5 is most verbose
 */
// prefix, level, format, argument_list...
function log_debug2()
{
	// Grab function arguments
	$args = func_get_args();
	$prefix = array_shift($args);
	$level = array_shift($args);
	$format = array_shift($args);

	$severities = array(
			1 => "ERROR",
			2 => "WARNING",
			3 => "NOTICE",
			4 => "INFO",
			5 => "DEBUG"
	);

	// Check if we have a log level defined
	if (!defined('AWIT_IPPM_LOG_LEVEL')) {
		return false;
	}

	// Setup a timestamp
	$timestamp = strftime('%Y-%m-%d %H:%M:%S', time());
	// Set severity
	$severity = $severities[$level];
	// Format message
	$message = vsprintf($format,$args);

	// If we have a file, log to file, or log to stderr
	if (defined('AWIT_IPPM_LOG_FILE') && AWIT_IPPM_LOG_FILE) {
		if ($level <= AWIT_IPPM_LOG_LEVEL) {
			$fp = fopen(AWIT_IPPM_LOG_FILE,'a');
			fwrite($fp,"$timestamp [$prefix] $severity $message\n");
			fflush($fp);
		}
	} else {
		$fp = fopen('/dev/stderr','a');
		fwrite($fp,"$timestamp [$prefix] $severity $message\n");
		fflush($fp);
		fclose($fp);
	}
}



/*
 * Function to display date in a pretty way
 */
function log_debug2_pretty_date($date)
{
	return isset($date) ? (gettype($date) == "object" ? $date->format('Y-m-d') : $date) : '-';
}



/*
 * Function to display a variable in a pretty way
 */
function log_debug2_pretty_variable($var)
{
	return isset($var) ? $var : '-';
}



/*
 * Function to initialize the module and pull in the custom fields
 */
function awit_ippm_init()
{
	global $AWIT_IPPM_IVER;
	global $AWIT_IPPM_ADMIN_USER;
	global $AWIT_IPPM_CONFIG_CUSTOM_FIELDS;
	global $AWIT_IPPM_CONFIG_CUSTOM_FIELDS_REVERSE;
	global $AWIT_IPPM_SUPPORTED_FIELDS;


	$config = _awit_ippm_config();

	// Dynamically create our field config
	foreach ($config['fields'] as $fieldName => $field) {
		if (isset($field['awitippm_ctype'])) {
			$AWIT_IPPM_SUPPORTED_FIELDS[$fieldName] = $field;
		}
		$AWIT_IPPM_CONFIG_CUSTOM_FIELDS[$fieldName] = $field['Default'];
	}
	$AWIT_IPPM_SUPPORTED_FIELDS['version'] = false;


	// Query modules table
	$result = select_query("tbladdonmodules","setting,value",array( 'module' => 'awit_ippm' ));

	// Filter out the settings we need
	while ($row = mysql_fetch_assoc($result)) {
		// Check in our global list
		if (isset($AWIT_IPPM_SUPPORTED_FIELDS[$row['setting']])) {
			$value = trim($row['value']);
			// Setup globals
			$AWIT_IPPM_CONFIG_CUSTOM_FIELDS[$row['setting']] = $value;
		}
	}

	// Create reverse map
	foreach ($AWIT_IPPM_CONFIG_CUSTOM_FIELDS as $key => $value) {
		// Skip version
		if ($key == "version") {
			continue;
		}

		$AWIT_IPPM_CONFIG_CUSTOM_FIELDS_REVERSE[$value] = $key;
	}

	return $config;
}



/**
 * Bills client a value based on a variable identifier, value or quantity
 */
function awit_ippm_doVariableBilling($invoice_id, $live)
{
	global $AWIT_IPPM_ADMIN_USER;
	global $AWIT_IPPM_CONFIG_CUSTOM_FIELDS;


	log_debug2('VBILLING',3,'Processing invoice [%s] for [VariableBilling], live [%s]',$invoice_id,$live);

	// Grab the invoice
	$invoice = localAPI('getinvoice',array('invoiceid' => $invoice_id),$AWIT_IPPM_ADMIN_USER);

	// We cache our invoice items between the two loops
	$invoiceData = array();

	// Check if we doing commitment billing
	$hasVariableBillingField = 0;
	foreach ($invoice['items']['item'] as $invoiceItemKey => $invoiceItem) {
		// Grab product info
		$invoiceData[$invoiceItemKey] = list($cproduct,$customFields,$a_invoiceItem) = awit_ippm_getProductInfo($invoiceItem);
		// Flip the flag if we're doing variable billing
		if (array_key_exists('variable_amount',$customFields) || array_key_exists('variable_quantity',$customFields)) {
			$hasVariableBillingField = 1;
		}
	}
	// If we not there is no use continuing
	if (!$hasVariableBillingField) {
		log_debug2('VBILLING',4,'Skipping invoice, no variable billing on this invoice');
		return;
	}

	// Arrays of our product updates so we process them in 1 go right at the end
	$cproductUpdates = array();
	$cproductCustomFieldUpdates = array();

	// Updates to process for this invoice
	$invoiceUpdates = array();

	// Loop with invoice items
	foreach ($invoiceData as $invoiceItemData) {
		// Grab items we need
		list($cproduct,$customFields,$invoiceItem) = $invoiceItemData;

		// Save original invoice item
		$origInvoiceItem = $invoiceItem;

		// If we have a supported variable field, update the line item accordingly
		// if not check the next item
		if (
				!isset($customFields['variable_quantity']) &&
				!isset($customFields['variable_amount']) &&
				!isset($customFields['variable_identifier']) &&
				!isset($customFields['variable_attributes'])
		) {
			log_debug2('VBILLING',4,'Skipping for line item, no variable billing fields set');
			continue;
		}

		// Check Variable Quantity
		$variableQuantity = NULL;
		if ($value = $customFields['variable_quantity']) {
			// Check if its valid
			if (preg_match('/^[0-9\.]+$/',$value)) {
				// Pull in variable quantity
				$variableQuantity = $value;
			} elseif ($value == "n/a") {
			} else {
				log_debug2('VBILLING',1,'Validation on field [%s] value [%s] failed, removing line item',
					$AWIT_IPPM_CONFIG_CUSTOM_FIELDS['variable_quantity'],
					log_debug2_pretty_variable($value)
				);
				// Remove from invoice
				$invoiceUpdates['deletelineids'][] = $invoiceItem['id'];
				continue;
			}
		}

		// Check Variable Amount
		$variableAmount = NULL;
		if ($value = $customFields['variable_amount']) {
			//check for a floating point number
			if (preg_match('/^[0-9\.]+$/',$value)) {
				// Pull in variable amount
				$variableAmount = $value;
			} elseif ($value == "n/a") {
			} else {
				log_debug2('VBILLING',1,'Validation on field [%s] value [%s] failed, removing line item',
					$AWIT_IPPM_CONFIG_CUSTOM_FIELDS['variable_amount'],
					log_debug2_pretty_variable($value)
				);
				// Remove from invoice
				$invoiceUpdates['deletelineids'][] = $invoiceItem['id'];
				continue;
			}
		}

		// If we cannot use the variable amount or quantity, we cannot do anything
		if (empty($variableAmount) && empty($variableQuantity)) {
			log_debug2('VBILLING',1,'Neither [%s] nor [%s] can be used, removing line item',
				$AWIT_IPPM_CONFIG_CUSTOM_FIELDS['variable_quantity'],
				$AWIT_IPPM_CONFIG_CUSTOM_FIELDS['variable_amount']
			);
			// Remove from invoice
			$invoiceUpdates['deletelineids'][] = $invoiceItem['id'];
			continue;
		}

		// Check Variable Identifier
		$variableIdentifier = NULL;
		if ($value = $customFields['variable_identifier']) {
			// Check for a valid identifier e.g. [system=radius,type=usage,index=realm,realm=rsadsl,class=shaped]
			if (preg_match('/^\[(.*)=(.*)[,]?(.*)\]$/',$value)) {
				// Pull in variable identifier
				$variableIdentifier = $value;
			} else {
				log_debug2('VBILLING',1,'Validation on field [%s] value [%s] failed, removing line item',
					$AWIT_IPPM_CONFIG_CUSTOM_FIELDS['variable_identifier'],
					log_debug2_pretty_variable($value)
				);
				// Remove from invoice
				$invoiceUpdates['deletelineids'][] = $invoiceItem['id'];
				continue;
			}
		}

		// Check Variable Attributes
		$variableAttributesStr = NULL;
		if ($value = $customFields['variable_attributes']) {
			// Check for a valid Attributes e.g. [round=alwaysup]
			if (preg_match('/^\[(.*=.*,?.*)\]$/',$value,$matches)) {
				// Pull in variable attributes
				$variableAttributeStr = $matches[1];
			} else {
				log_debug2('VBILLING',1,'Validation on field [%s] value [%s] failed, removing line item',
					$AWIT_IPPM_CONFIG_CUSTOM_FIELDS['variable_attributes'],
					log_debug2_pretty_variable($value)
				);
				// Remove from invoice
				$invoiceUpdates['deletelineids'][] = $invoiceItem['id'];
				continue;
			}
		}

		// We cannot work if there are unset items
		if ((isset($variableAmount) && isset($variableQuantity))) {
			log_debug2('VBILLING',1,'Validation on field combination [%s][%s], [%s][%s], [%s][%s] failed',
				log_debug2_pretty_variable($AWIT_IPPM_CONFIG_CUSTOM_FIELDS['variable_quantity']),
				log_debug2_pretty_variable($variableQuantity),
				log_debug2_pretty_variable($AWIT_IPPM_CONFIG_CUSTOM_FIELDS['variable_amount']),
				log_debug2_pretty_variable($variableAmount),
				log_debug2_pretty_variable($AWIT_IPPM_CONFIG_CUSTOM_FIELDS['variable_identifier']),
				log_debug2_pretty_variable($variableIdentifier)
			);
			// Remove from invoice
			$invoiceUpdates['deletelineids'][] = $invoiceItem['id'];
			continue;
		}

		// Parsing variable attributes
		$variableAttributes = array();
		$variableAttributeList = explode(',', $variableAttributeStr);
		foreach ($variableAttributeList as $variableAttribute) {
			// Check if we have an attribute, if not loop
			if (empty($variableAttribute)) {
				continue;
			}

			// Split off attribute name and value
			list($attr,$value) = explode('=', $variableAttribute);

			// Check attribute is valid
			if (!isset($attr) || !isset($value)) {
				log_debug2('VBILLING',1,'Variable attribute [%s] is invalid, removing line item',
					$variableAttribute
				);
				// Remove from invoice
				$invoiceUpdates['deletelineids'][] = $invoiceItem['id'];
				goto LOOP_END;
			}
			// Check for duplicates
			if (in_array($attr,array_keys($variableAttributes))) {
				log_debug2('VBILLING',1,'Variable attribute [%s] is duplicated, removing line item',
					$variableAttribute
				);
				// Remove from invoice
				$invoiceUpdates['deletelineids'][] = $invoiceItem['id'];
				goto LOOP_END;
			}

			$variableAttributes[strtolower(trim($attr))] = trim($value);
		}

		// Check if we processing a variable amount
		if (isset($variableAmount)) {
			$variableAmountOrig = $variableAmount;

			// Set default decimal places
			$decimals = 2;
			if (isset($variableAttributes['decimals'])) {
				$decimals = $variableAttributes['decimals'];
			}
			// Do our awesome rounding
			$decimalMultiplier = pow(10,$decimals);
			$variableAmount *= $decimalMultiplier;
			if (isset($variableAttributes['round'])) {
				// Check and do rounding
				if ($variableAttributes['round'] == "up") {
					$variableAmount = round($variableAmount, 0, PHP_ROUND_HALF_UP);
				} elseif ($variableAttributes['round'] == "down") {
					$variableAmount = round($variableAmount, 0, PHP_ROUND_HALF_DOWN);
				} elseif ($variableAttributes['round'] == "alwaysup") {
					$variableAmount = ceil($variableAmount);
				} elseif ($variableAttributes['round'] == "alwaysdown") {
					$variableAmount = floor($variableAmount);
				} else {
					log_debug2('VBILLING',1,'Variable amount cannot be rounded, rounding type is invalid [%s], removing line item',
						$variableAttributes['round']
					);
					// Remove from invoice
					$invoiceUpdates['deletelineids'][] = $invoiceItem['id'];
					continue;
				}
			}
			$variableAmount /= $decimalMultiplier;

			$newAmount = sprintf("%.2f",$variableAmount);

			$itemDescription = sprintf("\nVariable Amount: %s",$newAmount);

			log_debug2('VBILLING',4,'Variable attributes Round [%s], VariableAmountOrig [%s], Decimals [%s], VariableAmount [%s]',
				$variableAttributes['round'],
				$variableAmountOrig,
				$decimals,
				$variableAmount
			);

			// Push the changes to update
			$invoiceUpdates['itemdescription'][$invoiceItem['id']] = $itemDescription;
			$invoiceUpdates['itemamount'][$invoiceItem['id']] = $newAmount;
			$invoiceUpdates['itemtaxed'][$invoiceItem['id']] = $invoiceItem['taxed'];

			log_debug2('VBILLING',3,'~ LineItem[%s]: Description [%s] was [%s]',
				$invoiceItem['id'],
				str_replace("\n", '\n', $invoiceUpdates['itemdescription'][$invoiceItem['id']]),
				str_replace("\n", '\n', $origInvoiceItem['description'])
			);
			log_debug2('VBILLING',3,'~ LineItem[%s]: Amount [%s] was [%s]',
				$invoiceItem['id'],
				$invoiceUpdates['itemamount'][$invoiceItem['id']],
				$origInvoiceItem['amount']
			);

		// Do quantity based billing
		} elseif (isset($variableQuantity)) {
			$variableQuantityOrig = $variableQuantity;

			// Set default decimal places
			$decimals = 0;
			if (isset($variableAttributes['decimals'])) {
				$decimals = $variableAttributes['decimals'];
			}
			// Do our awesome rounding
			$decimalMultiplier = pow(10,$decimals);
			$variableQuantity *= $decimalMultiplier;
			if (isset($variableAttributes['round'])) {
				// Check and do rounding
				if ($variableAttributes['round'] == "up") {
					$variableQuantity = round($variableQuantity, 0, PHP_ROUND_HALF_UP);
				} elseif ($variableAttributes['round'] == "down") {
					$variableQuantity = round($variableQuantity, 0, PHP_ROUND_HALF_DOWN);
				} elseif ($variableAttributes['round'] == "alwaysup") {
					$variableQuantity = ceil($variableQuantity);
				} elseif ($variableAttributes['round'] == "alwaysdown") {
					$variableQuantity = floor($variableQuantity);
				} else {
					log_debug2('VBILLING',1,'Variable quantity cannot be rounded, rounding type is invalid [%s], removing line item',
						$variableAttributes['round']
					);
					// Remove from invoice
					$invoiceUpdates['deletelineids'][] = $invoiceItem['id'];
					continue;
				}
			}
			$variableQuantity /= $decimalMultiplier;

			$newAmount = sprintf("%.2f",($cproduct['recurringamount'] * $variableQuantity));

			// Set the unit to use for the quantity
			$unit = "";
			if (!empty($variableAttributes['unit'])) {
				$unit = str_replace("_"," ",$variableAttributes['unit']);
			}

			$itemDescription = sprintf("%s\nVariable Calculation: %.${decimals}f${unit} @%.2f",
					$invoiceItem['description'],
					$variableQuantity,
					$cproduct['recurringamount']
			);

			log_debug2('VBILLING',4,'Variable attributes Round [%s], VariableQuantityOrig [%s], Decimals [%s], VariableQuantity [%s], RecurringAmount [%s], NewAmount [%s]',
				$variableAttributes['round'],
				$variableQuantityOrig,
				$decimals,
				$variableQuantity,
				$cproduct['recurringamount'],
				$newAmount
			);

			// Push the changes to update
			$invoiceUpdates['itemdescription'][$invoiceItem['id']] = $itemDescription;
			$invoiceUpdates['itemamount'][$invoiceItem['id']] = $newAmount;
			$invoiceUpdates['itemtaxed'][$invoiceItem['id']] = $invoiceItem['taxed'];

			log_debug2('VBILLING',3,'~ LineItem[%s]: Description [%s] was [%s]',
				$invoiceItem['id'],
				str_replace("\n", '\n', $invoiceUpdates['itemdescription'][$invoiceItem['id']]),
				str_replace("\n", '\n', $origInvoiceItem['description'])
			);
			log_debug2('VBILLING',3,'~ LineItem[%s]: Amount [%s] was [%s]',
				$invoiceItem['id'],
				$invoiceUpdates['itemamount'][$invoiceItem['id']],
				$origInvoiceItem['amount']
			);
		}

LOOP_END:
	}

	awit_ippm_updateInvoice($invoice_id,$invoiceUpdates,$live);

	/*
	 * Update any fields for products that need to like billing start after the addons have been processed.
	 * We MUST do this AFTER we've processed the entire invoice above as addon ordering is not guaranteed
	 */
	foreach ($invoice['items']['item'] as $invoiceItemKey => $invoiceItem) {
		// Grab product info
		list($cproduct,$customFields) = awit_ippm_getProductInfo($invoiceItem);

		// Skip if there was a problem and we removing this item from the invoice, we want this to run again
		if (in_array($invoiceUpdates['deletelineids'],$invoiceItem['id'])) {
			log_debug2('VBILLING',4,'No changes to product as invoice line item is set to be deleted, skipping');
			continue;
		}

		$customFieldIDUpdates = array( );

		foreach ($cproductCustomFieldUpdates[$invoiceItemKey] as $name => $value) {
			// Set field ID
			$fieldID = $customFields['_'.$name.'_id'];

			log_debug2('VBILLING',4,'Update client product custom field - FieldName [%s], ServiceID [%s], FieldID [%s], OldValue [%s], NewValue [%s]',
				$name,$invoiceItem['relid'],$fieldID,$customFields[$name],$value
			);

			// Set custom field ID to update
			$customFieldIDUpdates[$fieldID] = $value;
		}

		// Process product changes
		$values = $cproductUpdates[$invoiceItemKey];
		$values['serviceid'] = $invoiceItem['relid'];
		$values['customfields'] = base64_encode(serialize($customFieldIDUpdates));
		if ($live) {
			$res = localAPI('updateclientproduct',$values,$AWIT_IPPM_ADMIN_USER);
			log_debug2('VBILLING',5,'Updated product: '.var_export($res,true));
		} else {
			log_debug2('VBILLING',3,'Non-Live Product Update: '.var_export($values,true));
		}
	}

	log_debug2('VBILLING',3,'Processing [VariableBilling] completed on invoice [%s]',$invoice_id);
}



/**
 * Performs prepaid, current and postpaid billing.
 * Adds back billing line items where needed.
 */
function awit_ippm_doCustomBilling($invoice_id, $live)
{
	global $AWIT_IPPM_ADMIN_USER;
	global $AWIT_IPPM_CONFIG_CUSTOM_FIELDS;


	log_debug2('BILLING',3,'Processing invoice [%s] for [CustomBilling], live [%s]',$invoice_id,$live);

	// Grab the invoice
	$invoice = localAPI('getinvoice',array('invoiceid' => $invoice_id),$AWIT_IPPM_ADMIN_USER);

	// Arrays of our product updates so we process them in 1 go right at the end
	$cproductUpdates = array();
	$cproductCustomFieldUpdates = array();

	// Updates to process for this invoice
	$invoiceUpdates = array();

	// Track the invoice date and due date
	// - Both should be the earliest dates
	$trackedInvoiceDate = NULL;
	$trackedInvoiceDueDate = NULL;


	// Loop with invoice items
	foreach ($invoice['items']['item'] as $invoiceItemKey => $invoiceItem) {
		// Grab product info
		list($cproduct,$customFields) = awit_ippm_getProductInfo($invoiceItem);

		// Check if we have a supported field
		if (!isset($customFields['billing_type'])) {
			log_debug2('BILLING',4,'Skipping for line item, no billing fields set');
			continue;
		}

		// Extra text to add to description
		$extraDesc = "";

		// Handling the products addons similarly to the actual product
		// This allows us to include the addons in our billing calculations
		if (strtolower($invoiceItem['type']) == 'addon') {
			// grabbing the connecting products hosting id
			$query = 'SELECT hostingid FROM tblhostingaddons WHERE id = ' . $invoiceItem['relid'] . '';
			$res = mysql_query($query);
			$row = mysql_fetch_assoc($res);
			// faking the relid here so the addon products will be treated like the tblhosting products
			// with access to all the custom fields and billing features for custom billing.
			$invoiceItem['relid'] = $row['hostingid'];
			log_debug2('BILLING',5,'Found addon, faking relid [%s]',$row['hostingid']);
		}

		// Save original invoice item
		$origInvoiceItem = $invoiceItem;

		// Client product details
		$cproduct_regdate = new DateTime($cproduct['regdate']);
		// Check billing start, check for override by custom field
		$cproduct_nextduedate = new DateTime($cproduct['nextduedate']);
		$newNextDueDate = NULL;

		// Shove in our token, so we can replace parts of the description later
		$cproduct_name = preg_replace('/^(.*)\(\d{4}-\d{2}-\d{2} - \d{4}-\d{2}-\d{2}\)(.*)$/m','$1@@@$2',$invoiceItem['description']);

		// Billing cycle
		if ($cproduct['billingcycle'] == "Monthly") {
			$billingCycle = 1;
		} elseif ($cproduct['billingcycle'] == "Quarterly") {
			$billingCycle = 3;
		} elseif ($cproduct['billingcycle'] == "Semi-Annually") {
			$billingCycle = 6;
		} elseif ($cproduct['billingcycle'] == "Annually") {
			$billingCycle = 12;
		} elseif ($cproduct['billingcycle'] == "Biennially") {
			$billingCycle = 24;
		} elseif ($cproduct['billingcycle'] == "Triennially") {
			$billingCycle = 36;
		} else {
			log_debug2('BILLING',1,'Unknown billing cycle "%s", removing line item', $cproduct['billingcycle']);
			// Check if we must remove the line or not
			$invoiceUpdates['deletelineids'][] = $invoiceItem['id'];
			goto LOOP_END;
		}

		// Look for the stuff we support
		$billingType = strtolower($customFields['billing_type']);

		// If unsupported, err and skip
		if (!in_array($billingType, array('prepaid', 'postpaid', 'current'))) {
			log_debug2('BILLING',1,'Unknown billing type "%s", removing line item', $billingType);
			// Check if we must remove the line or not
			$invoiceUpdates['deletelineids'][] = $invoiceItem['id'];
			continue;
		}

		// Check signup date
		$signupDate = NULL;
		if ($value = $customFields['signup_date']) {
			if (preg_match('/^\d{4}-\d{2}-\d{2}$/',$value)) {
				// Pull in billing start date
				$signupDate = new DateTime($value);
			// Special case to set product regdate
			} elseif (preg_match('/^REG(\d{4}-\d{2}-\d{2})$/',$value,$matches)) {
				// Pull in billing start date
				$cproduct_regdate = new DateTime($matches[1]);
				$signupDate = "";
				log_debug2('BILLING',4,'  - Setting RegDate [%s]',log_debug2_pretty_date($cproduct_regdate));
			// Ignore n/a
			} elseif ($value == "n/a") {
			} else {
				log_debug2('BILLING',1,'Validation on field [%s] value [%s] failed, removing line item',
					$AWIT_IPPM_CONFIG_CUSTOM_FIELDS['signup_date'],
					log_debug2_pretty_variable($value)
				);
				// Check if we must remove the line or not
				$invoiceUpdates['deletelineids'][] = $invoiceItem['id'];
				continue;
			}
		}

		// Check billing period start date
		if ($value = $customFields['billing_period_start']) {
			if (preg_match('/^\d{4}-\d{2}-\d{2}$/',$value)) {
				// Pull in billing period start date
				$cproduct_nextduedate = new DateTime($value);
			// Ignore n/a
			} elseif ($value == "n/a") {
			} else {
				log_debug2('BILLING',1,'Validation on field [%s] value [%s] failed, removing line item',
					$AWIT_IPPM_CONFIG_CUSTOM_FIELDS['billing_period_start'],
					log_debug2_pretty_variable($value)
				);
				// Check if we must remove the line or not
				$invoiceUpdates['deletelineids'][] = $invoiceItem['id'];
				continue;
			}
		}

		// Set the invoice include days
		$invoiceIncludeDays = $AWIT_IPPM_CONFIG_CUSTOM_FIELDS['invoice_include_days'];
		if (!isset($invoiceIncludeDays) || empty($invoiceIncludeDays) || $invoiceIncludeDays < 1) {
			$invoiceIncludeDays = 0;
		}

		// Get how signup is aligned to our billing run
		$signupAlignDays = NULL;
		if (!empty($signupDate)) {
			$signupAlignDays = $signupDate->diff($cproduct_nextduedate)->format('%R%a');
		}

		log_debug2('BILLING',4,'Service info - RegDate [%s], SignupDate [%s], NextDueDate [%s], BillingCycleMonths [%s], SignupAlignDays [%s], InvoiceIncludeDays [%s]',
			log_debug2_pretty_date($cproduct_regdate),
			log_debug2_pretty_date($signupDate),
			log_debug2_pretty_date($cproduct_nextduedate),
			$billingCycle,
			$signupAlignDays,
			$invoiceIncludeDays
		);

		// Check billing start
		$billingStartDate = NULL;
		if ($value = $customFields['billing_start']) {
			if (preg_match('/^\d{4}-\d{2}-\d{2}$/',$value)) {
				// Pull in billing start date
				$billingStartDate = new DateTime($value);
			// Ignore n/a
			} elseif ($value == "n/a") {
			} else {
				log_debug2('BILLING',1,'Validation on field [%s] value [%s] failed, removing line item',
					$AWIT_IPPM_CONFIG_CUSTOM_FIELDS['billing_start'],
					log_debug2_pretty_variable($value)
				);
				// Check if we must remove the line or not
				$invoiceUpdates['deletelineids'][] = $invoiceItem['id'];
				continue;
			}
		}

		// Check billing end
		$billingEndDate = NULL;
		if ($value = $customFields['billing_end']) {
			if (preg_match('/^\d{4}-\d{2}-\d{2}$/',$value)) {
				// Pull in billing end date
				$billingEndDate = new DateTime($value);
			// Ignore n/a
			} elseif ($value == "n/a") {
			} else {
				log_debug2('BILLING',1,'Validation on field [%s] value [%s] failed, removing line item',
					$AWIT_IPPM_CONFIG_CUSTOM_FIELDS['billing_end'],
					log_debug2_pretty_variable($value)
				);
				// Check if we must remove the line or not
				$invoiceUpdates['deletelineids'][] = $invoiceItem['id'];
				continue;
			}
		}

		log_debug2('BILLING',4,'Billing info - BillingType [%s], BillingStart [%s], BillingEndDate [%s]',
			$billingType,
			log_debug2_pretty_date($billingStartDate),
			log_debug2_pretty_date($billingEndDate)
		);

		// Setup the start & end dates based on billing cycle
		$billingPeriodStartDate = NULL; $billingPeriodEndDate = NULL;
		if ($billingType == "prepaid") {
			// If we have a billing start date use it, instead of using next due date, which would result in the start_date being in the past
			if ($billingStartDate) {
				$billingPeriodStartDate = clone $billingStartDate;
			} else {
				$billingPeriodStartDate = clone $cproduct_nextduedate;
			}
			// The billing cycle end date is the nextduedate plus one month plus the billing cycle
			$billingPeriodEndDate = clone $cproduct_nextduedate;
			$billingPeriodEndDate->modify("+$billingCycle month");
			// Next due date is the end of our billing period
			$newNextDueDate = clone $cproduct_nextduedate;
			$newNextDueDate->modify("+$billingCycle month");

		} elseif ($billingType == "current") {
			// If we have a billing start date use it, instead of using next due date, which would result in the start_date being in the past
			if ($billingStartDate) {
				$billingPeriodStartDate = clone $billingStartDate;
			} else {
				$billingPeriodStartDate = clone $cproduct_nextduedate;
				$billingPeriodStartDate->modify("-1 month");
			}

			// Start off by setting the end period to the next due date
			$billingPeriodEndDate = clone $cproduct_nextduedate;
			$billingPeriodEndDate->modify("-1 month");
			$billingPeriodEndDate->modify("+$billingCycle month");
			// Next due date is the end of our billing period
			$newNextDueDate = clone $cproduct_nextduedate;
			$newNextDueDate->modify("+$billingCycle month");

		} elseif ($billingType == "postpaid") {
			// If we have a specific start date use it
			if ($billingStartDate) {
				$billingPeriodStartDate = clone $billingStartDate;

			// Or assume we had one, we invoiced for it, now we doing a full month run
			} else {
				$billingPeriodStartDate = clone $cproduct_nextduedate;
				// Set back the start date to the next due date minus one billing cycle, and one month
				$billingPeriodStartDate->modify("-1 month");
				$billingPeriodStartDate->modify("-$billingCycle month");
			}

			// Set postpaid end date as next due date
			$billingPeriodEndDate = clone $cproduct_nextduedate;
			$billingPeriodEndDate->modify("-1 month");
			// Next due date is the end of our billing period
			$newNextDueDate = clone $cproduct_nextduedate;
			$newNextDueDate->modify("+$billingCycle month");
		}

		// If item passed until here do some validation
		if ($billingPeriodEndDate->diff($billingPeriodStartDate)->format('%R%a') >= 0) {
			// Failing that, this is a odd circumstance
			log_debug2('BILLING',1,'BillingPeriodStartDate [%s] greater than or equal to BillingPeriodEndDate [%s], removing line item',
				log_debug2_pretty_date($billingPeriodStartDate), log_debug2_pretty_date($billingPeriodEndDate)
			);
			// Check if we must remove the line or not
			$invoiceUpdates['deletelineids'][] = $invoiceItem['id'];
			goto LOOP_END;
		}

		// If we just signed up we probably need to calculate the invoice date
		if (!empty($signupDate)) {
			// If the tracked invoice date is not set, set it
			$invoiceDate = clone $signupDate;
			if (!isset($trackedInvoiceDate)) {
				log_debug2('BILLING',4,'  - Setting invoice date [%s]',
					log_debug2_pretty_date($invoiceDate)
				);
				$trackedInvoiceDate = $invoiceDate;
			// If its set, then ...
			} else {
				// Make sure that if we have a difference in dates which is positive we update it
				if ($invoiceDate->diff($trackedInvoiceDate)->format('%R%a') > 0) {
					log_debug2('BILLING',4,'  - Updating invoice date [%s] is sooner than [%s]',
						log_debug2_pretty_date($invoiceDate),
						log_debug2_pretty_date($trackedInvoiceDate)
					);
					$trackedInvoiceDate = $invoiceDate;
				}
			}
		}

		// If we don't have an invoice due date set, set it
		$invoiceDueDate = clone $cproduct_nextduedate;
		$invoiceDueDate->modify("-1 day");
		if (!isset($trackedInvoiceDueDate)) {
			log_debug2('BILLING',4,'  - Setting invoice due date [%s]',
				log_debug2_pretty_date($invoiceDueDate)
			);
			$trackedInvoiceDueDate = $invoiceDueDate;
		} else {
			// Make sure that if we have a difference in dates which is positive we update it
			if ($invoiceDueDate->diff($trackedInvoiceDueDate)->format('%R%a') > 0) {
				log_debug2('BILLING',4,'  - Updating invoice due date [%s] is sooner than [%s]',
					log_debug2_pretty_date($invoiceDueDate),
					log_debug2_pretty_date($trackedInvoiceDueDate)
				);
				$trackedInvoiceDueDate = $invoiceDueDate;
			}
		}

		// If our invoice due date is before our invoice date, reset it
		if (isset($trackedInvoiceDate)) {
			if ($trackedInvoiceDate->diff($trackedInvoiceDueDate)->format('%R%a') < 0) {
				$trackedInvoiceDueDate = clone $trackedInvoiceDate;
				log_debug2('BILLING',4,'  - Overriding invoice due date [%s]',
					log_debug2_pretty_date($trackedInvoiceDate)
				);
			}
		}

		// Set displayStartDate to start_date as a default value, this value will be changed by backbilling as we progress through billing cycles
		$displayStartDate = clone $billingPeriodStartDate;

		// Prime our loop
		$remainingBillingPeriods = 0;
		// We determine the billing cycle start date by seeing how many days there are to the billing period start date, if under 0, we found it
		$billingCycleStartDate = clone $billingPeriodEndDate;
		while ($billingPeriodStartDate->diff($billingCycleStartDate)->format('%R%a') > 0) {
			$billingCycleStartDate->modify("-$billingCycle month");
			$remainingBillingPeriods++;
		}
		// Naturally the billing end date would be the billing start date plus a billing cycle
		$billingCycleEndDate = clone $billingCycleStartDate;
		$billingCycleEndDate->modify("+$billingCycle month");
		log_debug2('BILLING',4,'  - NEXT => BillingCycleStartDate [%s], BillingCycleEndDate [%s], RemainingBillingPeriods [%s]',
			log_debug2_pretty_date($billingCycleStartDate),
			log_debug2_pretty_date($billingCycleEndDate),
			$remainingBillingPeriods
		);

		// Check if we need to override the billing period end date (if we have a billing end date specified that is)
		// Everything up till now assumes a full billing period, its easier to assume this till now, but now we notice its not
		if (isset($billingEndDate)) {
			// Check if the billing end date falls within the current period
			if ($billingEndDate->diff($billingPeriodEndDate)->format('%R%a')) {
				$billingPeriodEndDate = clone $billingEndDate;
			}
		}

		// Total days we going to be billing for, +1 is inclusive
		$totalBillingDays = $billingPeriodEndDate->diff($billingPeriodStartDate)->days + 1;

		log_debug2('BILLING',4,'Run info - PeriodStartDate [%s], PeriodEndDate [%s], TotalBillingDays [%s]',
			log_debug2_pretty_date($billingPeriodStartDate),
			log_debug2_pretty_date($billingPeriodEndDate),
			$totalBillingDays
		);

		/*
		 * billingPeriodStartDate
		 *  - The day from which we start billing the client
		 * billingPeriodEndDate
		 *  - The day to which we bill the client
		 *
		 * remainingBillingPeirods
		 *  - Additional billing periods left to bill for when we exit backbilling
		 *  - We start be decrementing this by 1, as the period we are IN does not count as an ADDITIONAL period
		 *
		 * billingCycleStartDate
		 *  - This is the start date of the CURRENT cycle we're looping with
		 *  - This is bumped at the end of the backbilling loop
		 * billingCycleEndDate
		 *  - This is the end date of the CURRENT cycle we're looping with
		 *  - This is also bumped at the end of the backbilling loop
		 *
		 * totalBillingCycleBillingDays
		 *  - This is the number of billing days between billing cycle start date and billing cycle end date
		 * billingPeriodStartToCycleOffset
		 *  - This is the number of days from the billing period start date to the CURRENT billing cycle start date
		 * billingPeriodEndToCycleOffset
		 *  - This is the number of days from the billing period end date to the CURRENT billing cycle end date
		 */

		$totalBillingCycleBillingDays = NULL;
		$billingPeriodStartToCycleOffset = NULL;
		$billingPeriodEndToCycleOffset = NULL;

		// We need to track the total billing amount for this line item to update 'firstpaymentamount' later
		$lineItemTotalAmount = 0;

		while (1) {
			// Calculate a bunch of offsets
			$totalBillingCycleBillingDays = $billingCycleStartDate->diff($billingCycleEndDate)->days;
			$billingPeriodStartToCycleOffset = $billingPeriodStartDate->diff($billingCycleStartDate)->format('%R%a');
			$billingPeriodEndToCycleOffset = $billingPeriodEndDate->diff($billingCycleEndDate)->format('%R%a');

			log_debug2('BILLING',4,
				'  - LOOP => TotalBillingCycleBillingDays [%s], BillingPeriodStartToCycleOffset [%s], BillingPeriodEndToCycleOffset, '.
					'[%s]',
				$totalBillingCycleBillingDays,
				$billingPeriodStartToCycleOffset,
				$billingPeriodEndToCycleOffset
			);

			// Check if we've hit our billing end cycle
			if ($billingPeriodEndToCycleOffset >= 0) {
				break;
			}

			/*
			 * BACKBILLING START
			 */

			// Start off will the full number of days
			$backBillingDays = $totalBillingCycleBillingDays;

			// Set display start date
			$displayStartDate = clone $billingCycleStartDate;
			// Set display end date
			$displayEndDate = clone $billingCycleEndDate;
			$displayEndDate->modify("-1 day");

			// If we starting later than the billing, we need to do a few adjustments
			if ($billingPeriodStartToCycleOffset < 0) {
				// The actual offset to add is the difference from the billing period start back to the billing cycle start
				// and this is a negative. We multiply it by -1 to get a positive and add it to the start date.
				$offset = $billingPeriodStartToCycleOffset * -1;
				$displayStartDate->modify("+$offset day");
				// We need to adjust the number of billing days aswell, remember the offset is negative
				$backBillingDays += $billingPeriodStartToCycleOffset;
			}
			// If we cutting short of the end of the cycle, adjust
//FIXME:			if ($billingPeriodEndToCycleOffset > 0) {
//				$displayEndDate->modify("-$billingPeriodEndToCycleOffset day");
//			}

			// Add backbilling line
			$invoiceUpdates['newitemdescription'][] = preg_replace(
				'/@@@/',
				"(BackBill: ".$displayStartDate->format('Y-m-d')." to ".$displayEndDate->format('Y-m-d').")",
				$cproduct_name
			);

			// Work out amount
			// XXX: We using invoiceItem['amount'] for the last billing item as this can come from somewhere else, like vbilling
			$backBillingAmount = sprintf('%.2f',
				$invoiceItem['amount'] * ($backBillingDays / $totalBillingCycleBillingDays)
			);
			$invoiceUpdates['newitemamount'][] = $backBillingAmount;

			// Bump our lineItemTotalAmount
			$lineItemTotalAmount += $backBillingAmount;

			// Make sure we inherit if its taxed
			$invoiceUpdates['newitemtaxed'][] = $invoiceItem['taxed'];

			log_debug2('BILLING',3,'+ BackBilling [%s] - amount [%s] * (backBillingDays [%s] / totalBillingCycleBillingDays [%s])',
				$backBillingAmount,
				$invoiceItem['amount'],
				$backBillingDays,
				$totalBillingCycleBillingDays
			);
			log_debug2('BILLING',3,'    - Description [%s]',
				str_replace("\n", '\n', $invoiceUpdates['newitemdescription'][count($invoiceUpdates['newitemdescription']) - 1])
			);

			/*
			 * BACKBILLING END
			 */

			// Next billing cycle
			$billingCycleStartDate->modify("+$billingCycle month");
			$billingCycleEndDate->modify("+$billingCycle month");
			$displayStartDate = clone $billingCycleStartDate;
			// Decrement remaining billing periods
			$remainingBillingPeriods--;

			log_debug2('BILLING',4,'  - NEXT => BillingCycleStartDate [%s], BillingCycleEndDate [%s]',
				log_debug2_pretty_date($billingCycleStartDate),
				log_debug2_pretty_date($billingCycleEndDate)
			);
		}
		/*
		 * END OF BACK BILLING
		 */

		// We use end_date - 1 day so it appears to be inclusive of the last day 2010-01-01 to 2010-01-31 instead of to 2010-02-01
		$displayEndDate = clone $billingPeriodEndDate;

		// only displaying previous months last day when billing ends on the 1st day of the month
		if ($billingPeriodEndDate->format('d') == '01') {
			$displayEndDate->modify("-1 day");
		}

		$extraDesc = sprintf("(%s: %s to %s)",ucfirst($billingType),$displayStartDate->format('Y-m-d'),$displayEndDate->format('Y-m-d'));

		// Lets make sure we have calculated the amount right, based on the number of days right for billing
		$lastBillingDays = $totalBillingCycleBillingDays;
		if ($billingPeriodStartToCycleOffset < 0) {
			$lastBillingDays += $billingPeriodStartToCycleOffset;
		}
		if ($billingPeriodEndToCycleOffset > 0) {
			$lastBillingDays -= $billingPeriodEndToCycleOffset;
		}

		// Calculate amount
		// XXX: We using invoiceItem['amount'] for the last billing item as this can come from somewhere else, like vbilling
		$lastBillingAmount = sprintf('%.2f',
			$backBillingAmount = ($invoiceItem['amount'] * ($lastBillingDays / $totalBillingCycleBillingDays)
		));
		$invoiceItem['amount'] = $lastBillingAmount;

		// Bump our lineItemTotalAmount
		$lineItemTotalAmount += $lastBillingAmount;

		/*
		 * Sales Features
		 */

		// Some vars we need
		$salesPerson = $customFields['sales_person'];
		$salesCommission = NULL;
		// Work out salesman comission
		if ($value = $customFields['sales_commission']) {
			if ($value > 0) {
				$salesCommission = sprintf("%.2f",$invoiceItem['amount'] * ($value / 100));
			}
		}

		// Do we have sales comission?
		if ($salesPerson && $salesCommission) {

			// Record these
			$table = "mod_awit_ippm";
			$values = array(
				"timestamp" => date("Y-m-d H:i:s"),
				"sales_person" => $salesPerson,
				"sales_commission" => $salesCommission,
				"invoice_id" => $invoiceItem['id']
			);
			$newid = insert_query($table,$values);

			// Mark as commission added
			$extraDesc .= "^";
		}

		/*
		 * Make final changes
		 */

		// Check if we must adjust the invoice item description
		if (strlen($extraDesc) > 0) {

			$invoiceUpdates['itemdescription'][$invoiceItem['id']] = preg_replace('/@@@/',$extraDesc,$cproduct_name);
			$invoiceUpdates['itemamount'][$invoiceItem['id']] = $invoiceItem['amount'];
			$invoiceUpdates['itemtaxed'][$invoiceItem['id']] = $invoiceItem['taxed'];

			log_debug2('BILLING',3,'~ LineItem[%s]: Description [%s] was [%s]',
				$invoiceItem['id'],
				str_replace("\n", '\n', $invoiceUpdates['itemdescription'][$invoiceItem['id']]),
				str_replace("\n", '\n', $origInvoiceItem['description'])
			);
			log_debug2('BILLING',3,'~ LineItem[%s]: Amount [%s] was [%s]',
				$invoiceItem['id'],
				$invoiceUpdates['itemamount'][$invoiceItem['id']],
				$origInvoiceItem['amount']
			);
		}


		// XXX: Lets keep product updates in one place
		// Reset our custom fields
		$cproductCustomFieldUpdates[$invoiceItemKey]['billing_period_start'] = "n/a";
		// NOTE!! - This means the service is terminating before the end cycle, we need to cancel it!
		if ($billingPeriodEndToCycleOffset > 0) {
			$cproductUpdates[$invoiceItemKey]['status'] = "cancelled";
			$cproductCustomFieldUpdates[$invoiceItemKey]['billing_period_end'] = "n/a";
			// Override next due date so we don't change it
			$newNextDueDate = $cproduct_nextduedate;
			log_debug2('BILLING',3,'~ LineItem[%s]: Status [%s] (overriding NewNextDueDate [%s])',
				$invoiceItem['id'],$cproductUpdates[$invoiceItemKey]['status'],log_debug2_pretty_date($newNextDueDate)
			);
		}
		// If we have a billing start date, it means this is the first invoice going out, set the first payment amount
		if (isset($billingStartDate)) {
			$cproductUpdates[$invoiceItemKey]['firstpaymentamount'] = $lineItemTotalAmount;
			$cproductCustomFieldUpdates[$invoiceItemKey]['billing_start'] = "n/a";
			log_debug2('BILLING',3,'~ LineItem[%s]: FirstPaymentAmount [%s]',
				$invoiceItem['id'],$lineItemTotalAmount
			);
		}

		// If we were the result of a signup, blank the signup date
		if (isset($signupDate)) {
			$cproductCustomFieldUpdates[$invoiceItemKey]['signup_date'] = "n/a";
		}

LOOP_END:
		// If we have a signup date, use it first
		if (isset($signupDate)) {

			// We have a signup date
			if (!empty($signupDate)) {
				$cproductUpdates[$invoiceItemKey]['regdate'] = $signupDate->format('Y-m-d');
				log_debug2('BILLING',3,'~ LineItem[%s]: RegDate [%s] (SignupDate)',
					$invoiceItem['id'],log_debug2_pretty_date($signupDate)
				);
			// Special regdate override for "REGXXXX-XX-XX" valus of field signup_date
			} else {
				$cproductUpdates[$invoiceItemKey]['regdate'] = $cproduct_regdate->format('Y-m-d');
				log_debug2('BILLING',3,'~ LineItem[%s]: RegDate [%s] (RegDate override)',
					$invoiceItem['id'],log_debug2_pretty_date($cproduct_regdate)
				);
			}
		// If we don't have a signup date revert to using our billing start date
		} elseif (isset($billingStartDate)) {
			$cproductUpdates[$invoiceItemKey]['regdate'] = $billingStartDate->format('Y-m-d');
			log_debug2('BILLING',3,'~ LineItem[%s]: RegDate [%s] (BillingStartDate)',
				$invoiceItem['id'],log_debug2_pretty_date($billingStartDate)
			);
		}

		// If we overriding the next due date, set it up nicely
		if (isset($newNextDueDate)) {
			$cproductUpdates[$invoiceItemKey]['nextduedate'] = $newNextDueDate->format('Y-m-d');
			log_debug2('BILLING',3,'~ LineItem[%s]: NextDueDate [%s]',
				$invoiceItem['id'],log_debug2_pretty_date($newNextDueDate)
			);
		}
	}

	// Check if we should reset the invoice dates...
	if (isset($trackedInvoiceDate) && !empty($trackedInvoiceDate)) {
		log_debug2('BILLING',3,'~ InvoiceDate [%s]',
			log_debug2_pretty_date($trackedInvoiceDate)
		);

		$invoiceUpdates['date'] = $trackedInvoiceDate->format('Y-m-d');
	} else {
		$trackedInvoiceDate = new DateTime($invoice['date']);
	}

	if (isset($trackedInvoiceDueDate) && !empty($trackedInvoiceDueDate)) {
		log_debug2('BILLING',3,'~ InvoiceDueDate [%s]',
			log_debug2_pretty_date($trackedInvoiceDueDate)
		);

		$invoiceUpdates['duedate'] = $trackedInvoiceDueDate->format('Y-m-d');
	} else {
		$trackedInvoiceDueDate = new DateTime($invoice['duedate']);
	}

	// Lastly sanity check, make sure our invoice date is at least our due date
	if ($trackedInvoiceDueDate->diff($trackedInvoiceDate)->format('%R%a') > 0) {
		log_debug2('BILLING',3,'~ Sanity override InvoiceDate [%s]',
			log_debug2_pretty_date($trackedInvoiceDueDate)
		);
		$invoiceUpdates['date'] = $trackedInvoiceDueDate->format('Y-m-d');
	}

	// Change the invoice
	awit_ippm_updateInvoice($invoice_id,$invoiceUpdates,$live);

	/*
	 * Update any fields for products that need to like billing start after the addons have been processed.
	 * We MUST do this AFTER we've processed the entire invoice above as addon ordering is not guaranteed
	 */
	foreach ($invoice['items']['item'] as $invoiceItemKey => $invoiceItem) {
		// Grab product info
		list($cproduct,$customFields) = awit_ippm_getProductInfo($invoiceItem);

		// If we have a billing_type, this module is being used, if not continue
		$billingType = strtolower($customFields['billing_type']);
		if (!in_array($billingType, array('prepaid', 'postpaid', 'current'))) {
			continue;
		}

		// Skip if there was a problem and we removing this item from the invoice, we want this to run again
		if (in_array($invoiceUpdates['deletelineids'],$invoiceItem['id'])) {
			log_debug2('BILLING',4,'No changes to product as invoice line item is set to be deleted, skipping');
			continue;
		}

		$customFieldIDUpdates = array( );

		foreach ($cproductCustomFieldUpdates[$invoiceItemKey] as $name => $value) {
			// Set field ID
			$fieldID = $customFields['_'.$name.'_id'];

			log_debug2('BILLING',4,'Update client product custom field - FieldName [%s], ServiceID [%s], FieldID [%s], OldValue [%s], NewValue [%s]',
				$name,$invoiceItem['relid'],$fieldID,$customFields[$name],$value
			);

			// Set custom field ID to update
			$customFieldIDUpdates[$fieldID] = $value;
		}

		// Process product changes
		$values = $cproductUpdates[$invoiceItemKey];
		$values['serviceid'] = $invoiceItem['relid'];
		$values['customfields'] = base64_encode(serialize($customFieldIDUpdates));
		if ($live) {
			$res = localAPI('updateclientproduct',$values,$AWIT_IPPM_ADMIN_USER);
			log_debug2('BILLING',4,'Updated product: '.var_export($res,true));
		} else {
			log_debug2('BILLING',3,'Non-Live Product Update: '.var_export($values,true));
		}
	}

	log_debug2('BILLING',3,'Processing [CustomBilling] completed on invoice [%s]',$invoice_id);
}



/**
 * Ensures the client has a certain number of products in ratio, if not bill him extra
 */
function awit_ippm_doCommitmentBilling($invoice_id, $live)
{
	global $AWIT_IPPM_ADMIN_USER;
	global $AWIT_IPPM_CONFIG_CUSTOM_FIELDS;


	log_debug2('CBILLING',3,'Processing invoice [%s] for [CommitmentBilling], live [%s]',$invoice_id,$live);

	// Grab the invoice
	$invoice = localAPI('getinvoice',array('invoiceid' => $invoice_id),$AWIT_IPPM_ADMIN_USER);

	// We cache our invoice items between the two loops
	$invoiceData = array();

	// Check if we doing commitment billing
	$hasCommitmentCalculationField = 0;
	foreach ($invoice['items']['item'] as $invoiceItemKey => $invoiceItem) {
		// Grab product info
		$invoiceData[$invoiceItemKey] = list($cproduct,$customFields,$a_invoiceItem) = awit_ippm_getProductInfo($invoiceItem);
		// Flip the flag if we're doing commitment calculation billing
		if (array_key_exists('commitment_calculation',$customFields)) {
			$hasCommitmentCalculationField = 1;
		}
	}
	// If we not there is no use continuing
	if (!$hasCommitmentCalculationField) {
		log_debug2('CBILLING',4,'Skipping invoice, no commitment calculation billing on this invoice');
		return;
	}

	// Updates to process for this invoice
	$invoiceUpdates = array();

	// Loop with invoice items
	foreach ($invoiceData as $invoiceItemData) {
		// Grab items we need
		list($cproduct,$customFields,$invoiceItem) = $invoiceItemData;

		log_debug2('CBILLING',4,'Product: [%s], Domain: [%s]',$cproduct['name'],$cproduct['domain']);

		// Check if our commitment calculation is blank
		if (empty($customFields['commitment_calculation'])) {
			log_debug2('CBILLING',4,'Skipping for line item, commitment_calculation field is empty');
			continue;
		}

		// Skip if somethings wrong
		if (
			empty($customFields['commitment_calculation']) || (
				empty($customFields['commitment_value']) && empty($customFields['variable_quantity'])
			)
		) {
			log_debug2('CBILLING',1,'Validation on field combination [%s][%s], [%s][%s], [%s][%s] failed, removing line item',
				log_debug2_pretty_variable($AWIT_IPPM_CONFIG_CUSTOM_FIELDS['commitment_calculation']),
				log_debug2_pretty_variable($customFields['commitment_calculation']),
				log_debug2_pretty_variable($AWIT_IPPM_CONFIG_CUSTOM_FIELDS['commitment_value']),
				log_debug2_pretty_variable($customFields['commitment_value']),
				log_debug2_pretty_variable($AWIT_IPPM_CONFIG_CUSTOM_FIELDS['variable_quantity']),
				log_debug2_pretty_variable($customFields['variable_quantity'])
			);
			// Remove the line item
			$invoiceUpdates['deletelineids'][] = $invoiceItem['id'];
			continue;
		}

		// Save original invoice item
		$origInvoiceItem = $invoiceItem;

		// Check Variable Quantity
		$variableQuantity = NULL;
		if ($value = $customFields['variable_quantity']) {
			// Check if its valid
			if (preg_match('/^[0-9.]+$/',$value)) {
				// Grab variable quantity value
				$variableQuantity = $value;
			} else {
				log_debug2('CBILLING',1,'Validation on field [%s] with value [%s] failed, removing line item',
					log_debug2_pretty_variable($AWIT_IPPM_CONFIG_CUSTOM_FIELDS['variable_quantity']),
					log_debug2_pretty_variable($value)
				);
				// Remove the line item
				$invoiceUpdates['deletelineids'][] = $invoiceItem['id'];
				continue;
			}
		}

		// Check Commitment Value
		$commitmentValue = NULL;
		if ($value = $customFields['commitment_value']) {
			// Check for a floating point number
			if (preg_match('/^[+-]?[0-9]*\.?[0-9]*$/',$value)) {
				// Grab commitment value
				$commitmentValue = $value;
			} else {
				log_debug2('CBILLING',1,'Validation on field [%s] with value [%s] failed, removing line item',
					log_debug2_pretty_variable($AWIT_IPPM_CONFIG_CUSTOM_FIELDS['commitment_value']),
					log_debug2_pretty_variable($value)
				);
				// Remove the line item
				$invoiceUpdates['deletelineids'][] = $invoiceItem['id'];
				continue;
			}
		}

		// Check commitment caclculation
		$commitmentCalculation = NULL;
		if ($value = $customFields['commitment_calculation']) {
			// Check for a valid commitments
			// 65=0.25,66=0.33,69=1,70=1,71=1.5,72=1.5
			if (preg_match('/^\[((?:[0-9]+=[0-9.]+,?)+)\]$/',$value,$matches)) {
				// Pull in commitment calculation minus the []
				$commitmentCalculation = $matches[1];
			} else {
				log_debug2('CBILLING',1,'Validation on field [%s] value [%s] failed, removing line item',
					$AWIT_IPPM_CONFIG_CUSTOM_FIELDS['commitment_calculation'],
					log_debug2_pretty_variable($value)
				);
				// Remove the line item
				$invoiceUpdates['deletelineids'][] = $invoiceItem['id'];
				continue;
			}
		}

		log_debug2('CBILLING',4,'Service info - VariableQuantity [%s], CommitmentValue [%s], CommitmentCalculation [%s]',
			log_debug2_pretty_variable($variableQuantity),
			log_debug2_pretty_variable($commitmentValue),
			log_debug2_pretty_variable($commitmentCalculation)
		);

		// Check Variable Attributes
		$variableAttributesStr = NULL;
		if ($value = $customFields['variable_attributes']) {
			// check for a valid Attributes e.g. [round=alwaysup]
			if (preg_match('/^\[(.*=.*,?.*)\]$/',$value,$matches)) {
				// Pull in variable attributes
				$variableAttributeStr = $matches[1];
			} else {
				log_debug2('CBILLING',1,'Validation on field [%s] value [%s] failed, removing line item',
					$AWIT_IPPM_CONFIG_CUSTOM_FIELDS['variable_attributes'],
					log_debug2_pretty_variable($value)
				);
				// Remove from invoice
				$invoiceUpdates['deletelineids'][] = $invoiceItem['id'];
				continue;
			}
		}
		// Parsing variable attributes
		$variableAttributes = array();
		$variableAttributeList = explode(',', $variableAttributeStr);
		foreach ($variableAttributeList as $variableAttribute) {
			// Check if we have an attribute, if not loop
			if (empty($variableAttribute)) {
				continue;
			}

			// Split off attribute name and value
			list($attr,$value) = explode('=', $variableAttribute);

			// Check attribute is valid
			if (!isset($attr) || !isset($value)) {
				log_debug2('CBILLING',1,'Variable attribute [%s] is invalid, removing line item',
					$variableAttribute
				);
				// Remove from invoice
				$invoiceUpdates['deletelineids'][] = $invoiceItem['id'];
				goto LOOP_END;
			}
			// Check for duplicates
			if (in_array($attr,array_keys($variableAttributes))) {
				log_debug2('CBILLING',1,'Variable attribute [%s] is duplicated, removing line item',
					$variableAttribute
				);
				// Remove from invoice
				$invoiceUpdates['deletelineids'][] = $invoiceItem['id'];
				goto LOOP_END;
			}

			$variableAttributes[strtolower(trim($attr))] = trim($value);
		}


		// Mapping of pid's to multipliers
		$commitmentCalculationMapping = array();
		foreach (explode(',', $commitmentCalculation) as $item) {
			list($pid,$multiplier) = explode('=', $item);

			// Check attribute is valid
			if (!isset($pid) || !isset($multiplier)) {
				log_debug2('CBILLING',1,'Commitment calculation item [%s] is invalid, removing line item',
					$item
				);
				// Remove the line item
				$invoiceUpdates['deletelineids'][] = $invoiceItem['id'];
				goto LOOP_END;
			}
			// Check for duplicates
			if (in_array($pid,array_keys($commitmentCalculationMapping))) {
				log_debug2('CBILLING',1,'Commitment calculation item [%s] is duplicated, removing line item',
					$item
				);
				// Remove the line item
				$invoiceUpdates['deletelineids'][] = $invoiceItem['id'];
				goto LOOP_END;
			}

			$commitmentCalculationMapping[$pid] = $multiplier;
		}

		// Loop with invoice items and total up the total of the quantities multiplied by the multipliers
		$variableQuantityTotal = 0;
		foreach ($invoiceData as $v_invoiceItemData) {
			// Grab product info
			list($a_cproduct,$a_customFields,$a_cproduct) = $v_invoiceItemData;

			// Grab variable quantity
			$a_variableQuantity = NULL;
			$value = $customFields['variable_quantity'];
			// Check if its valid
			if (preg_match('/^[0-9.]+$/',$value)) {
				// Grab variable quantity value
				$a_variableQuantity = $value;
			} else {
				log_debug2('CBILLING',1,'Validation on field [%s] with value [%s] failed, ignoring',
					log_debug2_pretty_variable($AWIT_IPPM_CONFIG_CUSTOM_FIELDS['variable_quantity']),
					log_debug2_pretty_variable($value)
				);
				continue;
			}

			// Check Variable Attributes
			$a_variableAttributesStr = NULL;
			if ($value = $a_customFields['variable_attributes']) {
				// check for a valid Attributes e.g. [round=alwaysup]
				if (preg_match('/^\[(.*=.*,?.*)\]$/',$value,$matches)) {
					// Pull in variable attributes
					$a_variableAttributeStr = $matches[1];
				} else {
					log_debug2('CBILLING',1,'Validation on field [%s] value [%s] failed, ignoring',
						$AWIT_IPPM_CONFIG_CUSTOM_FIELDS['variable_attributes'],
						log_debug2_pretty_variable($value)
					);
					continue;
				}
			}

			// Parsing variable attributes
			$a_variableAttributes = array();
			$a_variableAttributeList = explode(',', $a_variableAttributeStr);
			foreach ($a_variableAttributeList as $a_variableAttribute) {
				// Check if we have an attribute, if not loop
				if (empty($a_variableAttribute)) {
					continue;
				}

				// Split off attribute name and value
				list($attr,$value) = explode('=', $a_variableAttribute);

				// Check attribute is valid
				if (!isset($attr) || !isset($value)) {
					log_debug2('CBILLING',1,'Variable attribute [%s] is invalid, ignoring',
						$a_variableAttribute
					);
					continue;
				}

				// Check for duplicates
				if (in_array($attr,array_keys($a_variableAttributes))) {
					log_debug2('CBILLING',1,'Variable attribute [%s] is duplicated, ignoring',
						$a_variableAttribute
					);
					continue;
				}

				$a_variableAttributes[strtolower(trim($attr))] = trim($value);
			}


			// If this is part of our calculation, then lets add it up
			if ($multiplier = $commitmentCalculationMapping[$a_cproduct['pid']]) {

				// Set default decimal places
				$decimals = 0;
				if (isset($a_variableAttributes['decimals'])) {
					$decimals = $a_variableAttributes['decimals'];
				}
				// Do our awesome rounding
				$decimalMultiplier = pow(10,$decimals);
				$a_variableQuantity *= $decimalMultiplier;
				if (isset($variableAttributes['round'])) {
					// Check and do rounding
					if ($variableAttributes['round'] == "up") {
						$a_variableQuantity = round($a_variableQuantity, 0, PHP_ROUND_HALF_UP);
					} elseif ($variableAttributes['round'] == "down") {
						$a_variableQuantity = round($a_variableQuantity, 0, PHP_ROUND_HALF_DOWN);
					} elseif ($variableAttributes['round'] == "alwaysup") {
						$a_variableQuantity = ceil($a_variableQuantity);
					} elseif ($variableAttributes['round'] == "alwaysdown") {
						$a_variableQuantity = floor($a_variableQuantity);
					} else {
						log_debug2('VBILLING',1,'Variable quantity cannot be rounded, rounding type is invalid [%s], removing line item',
							$variableAttributes['round']
						);
						// Remove from invoice
						$invoiceUpdates['deletelineids'][] = $invoiceItem['id'];
						continue;
					}
				}
				$a_variableQuantity /= $decimalMultiplier;

				// Calculate our new total
				$variableQuantityNewTotal = $variableQuantityTotal + ($a_variableQuantity * $multiplier);

				log_debug2('CBILLING',4,'Using [%s] domain [%s], VariableQuantityTotal [%s] = VariableQuantityTotal [%s] + (VariableQuantity [%s] * Multiplier [%s])',
					$a_cproduct['name'], $a_cproduct['domain'],
					$variableQuantityNewTotal,
					$variableQuantityTotal,
					$a_variableQuantity,
					$multiplier
				);

				// Save our new quantity
				$variableQuantityTotal = $variableQuantityNewTotal;
			}
		}

		// Only populate shortfall if its > 0
		$shortfall = $customFields['commitment_value'] - $variableQuantityTotal;
		$shortfall = ($shortfall > 0) ? $shortfall : 0;

		log_debug2('CBILLING',4,'Calculated VariableQuantityTotal [%s], Shortfall [%s]',
			$variableQuantityTotal,
			$shortfall
		);

		// If there is no shortfall, skip adding it to the invoice
		if (!$shortfall) {
			continue;
		}

		// Save shortfall value before rounding
		$shortfallOrig = $shortfall;

		// Set default decimal places
		$decimals = 0;
		if (isset($variableAttributes['decimals'])) {
			$decimals = $variableAttributes['decimals'];
		}
		// Do our awesome rounding
		$decimalMultiplier = pow(10,$decimals);
		$shortfall *= $decimalMultiplier;
		if (isset($variableAttributes['round'])) {
			// Check and do rounding
			if ($variableAttributes['round'] == "up") {
				$shortfall = round($shortfall, 0, PHP_ROUND_HALF_UP);
			} elseif ($variableAttributes['round'] == "down") {
				$shortfall = round($shortfall, 0, PHP_ROUND_HALF_DOWN);
			} elseif ($variableAttributes['round'] == "alwaysup") {
				$shortfall = ceil($shortfall);
			} elseif ($variableAttributes['round'] == "alwaysdown") {
				$shortfall = floor($shortfall);
			} else {
				log_debug2('VBILLING',1,'Variable quantity cannot be rounded, rounding type is invalid [%s], removing line item',
					$variableAttributes['round']
				);
				// Remove from invoice
				$invoiceUpdates['deletelineids'][] = $invoiceItem['id'];
				continue;
			}
		}
		$shortfall /= $decimalMultiplier;

		// Calculate item amount
		$itemAmount = sprintf("%.2f",($cproduct['recurringamount'] * $shortfall));

		// Set the unit to use for the quantity
		$unit = "";
		if (!empty($variableAttributes['unit'])) {
			$unit = str_replace("_"," ",$variableAttributes['unit']);
		}

		// If we do calculate the item data
		$itemDescription = sprintf("%s\nCommitment shortfall = %.${decimals}f$unit (Commitment %.${decimals}f$unit - Total %.${decimals}f$unit)",
			$invoiceItem['description'],
			$shortfall,
			$customFields['commitment_value'],
			$variableQuantityTotal
		);

		// Setup new item we're adding to the invoice
		$invoiceUpdates['newitemdescription'][] = $itemDescription;
		$invoiceUpdates['newitemamount'][] = $itemAmount;
		$invoiceUpdates['newitemtaxed'][] = $invoiceItem['taxed'];

		log_debug2('CBILLING',3,'+ Adding commitment ShortfallOrig [%s], Shortfall [%s], ItemAmount [%.2f], ItemDescription [%s]',
			$shortfallOrig,
			$shortfall,
			$itemAmount,
			str_replace("\n", '\n', $itemDescription)
		);

LOOP_END:
	}

	awit_ippm_updateInvoice($invoice_id,$invoiceUpdates,$live);

	log_debug2('CBILLING',3,'Processing [CommitmentBilling] completed on invoice [%s]',$invoice_id);
}



/**
 * Applies the bulk discount calculation to the specified products
 * where Bulk Discount Calculation is specified
 */
function awit_ippm_doBulkDiscountBilling($invoice_id, $live)
{
	global $AWIT_IPPM_ADMIN_USER;
	global $AWIT_IPPM_CONFIG_CUSTOM_FIELDS;


	log_debug2('BDBILLING',3,'Processing invoice [%s] for [BulkDiscountBilling], live [%s]',$invoice_id,$live);

	// Grab the invoice
	$invoice = localAPI('getinvoice',array('invoiceid' => $invoice_id),$AWIT_IPPM_ADMIN_USER);

	// We cache our invoice items between the two loops
	$invoiceData = array();

	// Check if we doing bulk discount billing
	$hasBulkDiscountField = 0;
	foreach ($invoice['items']['item'] as $invoiceItemKey => $invoiceItem) {
		// Grab product info
		$invoiceData[$invoiceItemKey] = list($cproduct,$customFields,$a_invoiceItem) = awit_ippm_getProductInfo($invoiceItem);
		// Return if we have the field we need
		if (array_key_exists('bulk_discount_calculation',$customFields)) {
			$hasBulkDiscountField = 1;
		}
	}
	// If we not there is no use continuing
	if (!$hasBulkDiscountField) {
		log_debug2('BDBILLING',1,'Skipping invoice, no bulk discount billing on this invoice');
		return;
	}

	// Updates to process for this invoice
	$invoiceUpdates = array();

	// Loop with invoice items
	foreach ($invoiceData as $invoiceItemData) {
		// Grab product info
		list($cproduct,$customFields,$invoiceItem) = $invoiceItemData;

		// Check if the bulk discount field is blank
		if (empty($customFields['bulk_discount_calculation'])) {
			log_debug2('BDBILLING',1,'Skipping for line item, bulk_discount_calculation field is empty');
			continue;
		}

		log_debug2('BDBILLING',4,'Product: [%s], Domain: [%s]',$cproduct['name'],$cproduct['domain']);

		// Check for bulk discount calculation field
		$bulkDiscountProducts = NULL;
		$bulkDiscountCalculation = NULL;
		if ($value = $customFields['bulk_discount_calculation']) {
			// check for a valid bulk discount calculation attribute pair string
			if (preg_match('/^\[((?:[0-9],?)+)\/((?:[0-9]+=[0-9\.]+,?)+)\]$/',$value,$matches)) {
				// Pull in bulk discount products & calculation
				$bulkDiscountProducts = $matches[1];
				$bulkDiscountCalculation = $matches[2];
			} else {
				log_debug2('BDBILLING',1,'Validation on field [%s] value [%s] failed, removing line item',
					log_debug2_pretty_variable($AWIT_IPPM_CONFIG_CUSTOM_FIELDS['bulk_discount_calculation']),
					log_debug2_pretty_variable($value)
				);
				// Remove from invoice
				$invoiceUpdates['deletelineids'][] = $invoiceItem['id'];
				continue;
			}
		}

		log_debug2('BDBILLING',4,'Service info - BulkDiscountCalculation [%s]',
			log_debug2_pretty_variable($bulkDiscountCalculation)
		);

		// Get an array with our products in
		$bulkDiscountProductList = explode(',',$bulkDiscountProducts);
		// Mapping of products to discount bands
		$bulkDiscountCalculationMapping = array();
		foreach (explode(',', $bulkDiscountCalculation) as $item) {
			list($quantity,$multiplier) = explode('=', $item);
			// Check attribute is valid
			if (!isset($quantity) || !isset($multiplier)) {
				log_debug2('BDBILLING',1,'Bulk discount calculation item [%s] is invalid',
					$item
				);
				// Remove from invoice
				$invoiceUpdates['deletelineids'][] = $invoiceItem['id'];
				continue;
			}
			// Check for duplicates
			if (in_array($quantity,array_keys($bulkDiscountCalculationMapping))) {
				log_debug2('BDBILLING',1,'Bulk discount calculation item [%s] is duplicated',
					$item
				);
				// Remove from invoice
				$invoiceUpdates['deletelineids'][] = $invoiceItem['id'];
				continue;
			}

			$bulkDiscountCalculationMapping[$quantity] = $multiplier;
		}
		// Sort array so we have the highest value is first
		krsort($bulkDiscountCalculationMapping);

		// Loop with invoice items and total up
		$variableQuantityTotal = 0;
		$variableAmountTotal = 0;
		foreach ($invoiceData as $v_invoiceItemData) {
			// Grab product info
			list($a_cproduct,$a_customFields,$a_invoiceItem) = $v_invoiceItemData;

			// If we have a specific quantity use it instead of the default 1
			$variableQuantity = 1;
			if (isset($a_customFields['variable_quantity'])) {
				$variableQuantity += $a_customFields['variable_quantity'];
			}
# FIXME: rounding
			// Calculate our new total
			$variableQuantityNewTotal = $variableQuantityTotal + $variableQuantity;
			$variableAmountNewTotal = $variableAmountTotal + $a_invoiceItem['amount'];

			log_debug2('BDBILLING',4,'Using [%s] domain [%s], VariableQuantityNewTotal updated [%s] = VariableQuantityTotal [%s] + VariableQuantity [%s], '.
					'VariableAmountNewTotal updated [%s] = VariableAmountTotal [%s] + VariableAmount [%s]',
				$a_cproduct['name'], $a_cproduct['domain'],
				$variableQuantityNewTotal,
				$variableQuantityTotal,
				$variableQuantity,
				$variableAmountNewTotal,
				$variableAmountTotal,
				$a_invoiceItem['amount']
			);
			// Save our values
			$variableQuantityTotal = $variableQuantityNewTotal;
			$variableAmountTotal = $variableAmountNewTotal;
		}

		log_debug2('BDBILLING',4,'Calculated VariableQuantityTotal [%s], VariableAmountTotal [%s]',
			$variableQuantityTotal.
			$variableAmountTotal
		);

		// Look which band this item falls in, start at the top
		foreach ($bulkDiscountCalculationMapping as $quantity => $multiplier) {
			// We found IT!!!
			if ($variableQuantityTotal >= $quantity) {
				// If we do calculate the item data
				$itemDescription = sprintf("%s\nBulk Discount of %.2f%% applied to %.2f",
					$invoiceItem['description'],
					$multiplier * 100,
					$variableAmountTotal
				);

				// Calculate the amount
				$itemAmount = sprintf('%.2f',($variableAmountTotal * $multiplier * -1));

				log_debug2('BDBILLING',3,'+ Adding commitment bulk discount with ItemDescription [%s] and ItemAmount [%.2f], DiscountBand [%s]',
					str_replace("\n", '\n', $itemDescription),
					$itemAmount,
					$quantity
				);

				break;
			}
		}

		// Setup new invoice item update
		$invoiceUpdates['newitemdescription'][$invoiceItem['id']] = $itemDescription;
		$invoiceUpdates['newitemamount'][$invoiceItem['id']] = $itemAmount;
		$invoiceUpdates['newitemtaxed'][$invoiceItem['id']] = $invoiceItem['taxed'];
	}

	awit_ippm_updateInvoice($invoice_id,$invoiceUpdates,$live);

	log_debug2('BDBILLING',3,'Processing [BulkDiscountBilling] completed on invoice [%s]',$invoice_id);
}



/*
 * Function to return product details
 * Returns: $cproduct, $configCustomFields, $product
 */
function awit_ippm_getProductInfo($invoiceItem)
{
	global $AWIT_IPPM_ADMIN_USER;
	global $AWIT_IPPM_CONFIG_CUSTOM_FIELDS_REVERSE;


	// Pull in the client product, we use ! to catch anything except > 0
	if (!$invoiceItem['relid']) {
		log_debug2('getProductInfo',4,'Skipping line item, relid not set');
		return;
	}

	$cproduct = localAPI('getclientsproducts',array('serviceid' => $invoiceItem['relid']),$AWIT_IPPM_ADMIN_USER);
	// Check if this is a product
	if (!isset($cproduct['products']['product'][0])) {
		return;
	}

	$cproduct = $cproduct['products']['product'][0];

	log_debug2('getProductInfo',4,'Product: [%s], Domain: [%s]',$cproduct['name'],$cproduct['domain']);

	// Make a list of custom fields
	$customFields = array();
	foreach ($cproduct['customfields']['customfield'] as $customField) {
		$customFieldName = trim($customField['name']);
		// Check if this is a system custom field, if so rename it
		if ($newCustomFieldName = $AWIT_IPPM_CONFIG_CUSTOM_FIELDS_REVERSE[$customFieldName]) {
			$customFieldName = $newCustomFieldName;
		}
		$customFields[$customFieldName] = trim($customField['value']);
		$customFields["_".$customFieldName."_id"] = $customField['id'];
	}

	// Pull in the system product
	$product = localAPI('getproducts',array('pid' => $cproduct['pid']),$AWIT_IPPM_ADMIN_USER);

	return array($cproduct, $customFields, $invoiceItem);
}



// Function to conditionally process invoice updates
function awit_ippm_updateInvoice($invoice_id,$invoiceUpdates,$live)
{
	global $AWIT_IPPM_ADMIN_USER;


	// Are there invoice updates to be made?
	if (sizeof($invoiceUpdates)) {
		// Change invoice
		if ($live) {
			// Parameters
			$params = $invoiceUpdates;
			$params['invoiceid'] = $invoice_id;
			// Update
			$res = localAPI('updateinvoice',$params,$AWIT_IPPM_ADMIN_USER);
		} else {
			log_debug2('awit_ippm_updateInvoice',3,'Non-Live Invoice Update: '.var_export($invoiceUpdates,true));
		}
	}
}



// Hook into the admin header output to add headers
function awit_ippm_hook_AdminAreaHeadOutput($vars)
{
	$head = "";

	// Abort if we're not running our module
	if (!preg_match('/addonmodules.php\?module=awit_ippm/',$_SERVER['REQUEST_URI'])) {
		return $head;
	}

	$head .= '<link href="../modules/addons/awit_ippm/awit_ippm_order.css" rel="stylesheet" type="text/css" />';
	$head .= '<script type="text/javascript" src="../modules/addons/awit_ippm/awit_ippm_order.js"></script>';
	return $head;
}



// Hook into the invoice creation so we can work some magic
function awit_ippm_hook_InvoiceCreationPreEmail($vars, $live = 1)
{
	global $AWIT_IPPM_IVER;
	global $AWIT_IPPM_CONFIG_CUSTOM_FIELDS;

	// Initialize module
	awit_ippm_init();

	// Check the addon is enabled?
	if ($live && $AWIT_IPPM_CONFIG_CUSTOM_FIELDS['version'] === false) {
		log_debug2('AWIT_IPPM',3,"Addon not enabled, live = $live");
		exit;
	}

	// Source of query
	$source = array_shift($vars);
	// User making query
	$user = array_shift($vars);
	// Invoice ID
	$invoice_id = array_shift($vars);

	log_debug2('AWIT_IPPM',3,'Version [%s]',$AWIT_IPPM_IVER);

	awit_ippm_doVariableBilling($invoice_id, $live);

	awit_ippm_doCustomBilling($invoice_id, $live);

	awit_ippm_doCommitmentBilling($invoice_id, $live);

	awit_ippm_doBulkDiscountBilling($invoice_id, $live);
}


// vim: ts=4
