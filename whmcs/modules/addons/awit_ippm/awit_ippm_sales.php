<?php
/*
 * AWIT IPPM - Sales page
 * Copyright (c) 2013-2014, AllWorldIT
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


// Make sure we not being accssed directly
if (!defined("WHMCS"))
	die("This file cannot be accessed directly");



// Addon output
function awit_ippm_output_sales($vars) {

	// Check if we have to mark items as paid
	if (isset($_POST['paid']) && is_array($_POST['paid'])) {

		// Query actions
		$table = "mod_awit_ippm";
		$update = array("paid"=>"1");
		$where = array();
		foreach ($_POST['paid'] as $id) {
			$where["id"] = $id;
		}

		// Do query
		update_query($table,$update,$where);
	}

	// Make link to use
	$link = $vars['modulelink'];

	// Default dates
	$dateFrom = new DateTime(date("Y-m"));
	$dateTo = clone $dateFrom;
	$dateTo->add(new DateInterval("P1M"));

	// Dates, formatted
	$dateFrom = $dateFrom->format("Y-m-d");
	$dateTo = $dateTo->format("Y-m-d");

	// Check if we have dates from and to to override defaults
	if (isset($_POST['date_from']) && isset($_POST['date_to'])) {

		// Try and convert to date time
		$postDateFrom = new DateTime($_POST['date_from']);
		$postDateTo = new DateTime($_POST['date_to']);

		// If successful, use them
		if (is_object($postDateFrom)) {
			$dateFrom = $postDateFrom->format("Y-m-d");
		}
		if (is_object($postDateTo)) {
			$dateTo = $postDateTo->format("Y-m-d");
		}
	}

	// Fancy date picker
	echo '<script>
		$(function() {
			$( "//date_from" ).datepicker({
				dateFormat: "yy-mm-dd",
				constrainInput: true
			});
			$( "//date_to" ).datepicker({
				dateFormat: "yy-mm-dd",
				constrainInput: true
			});
		});
	      </script>';

	// Date search fields
	echo "<p>Select a start and end date and hit search.</p>";
	echo "<form action='$link' method='post'>";
	echo "<input id='date_from' type='text' value='$dateFrom' name='date_from' />";
	echo "<input id='date_to' type='text' value='$dateTo' name='date_to' />";
	echo "<input type='submit' value='Search' />";
	echo "</form>";
	echo "<br /><br />";

	// Query the database
	$result = mysql_query("SELECT * FROM mod_awit_ippm WHERE timestamp >= '$dateFrom' AND timestamp <= '$dateTo'");

	// Loop through results and genenrate form
	$includeForm = 0;
	while ($row = mysql_fetch_array($result)) {

		// Open form
		if (!$includeForm) {
			$includeForm = 1;
			echo "<form action='".$link."' method='post' >";
			echo "<table border='1' cellpadding='10'>";
			echo "<tr>";
			echo "<th>Timestamp</th>";
			echo "<th>Invoice ID</th>";
			echo "<th>Sales Person</th>";
			echo "<th>Commission</th>";
			echo "<th>Status</th>";
			echo "</tr>";
		}

		// Nice pay status
		$payStatus = $row['paid'] ? "Paid" : "Unpaid";

		echo "<tr>";
		echo "<td>".$row['timestamp']."</td>";
		echo "<td>".$row['invoice_id']."</td>";
		echo "<td>".$row['sales_person']."</td>";
		echo "<td>".$row['sales_commission']."</td>";
		if ($payStatus == "Unpaid") {
			echo "<td>".$payStatus."  <input type='checkbox' name='paid[]' value='".$row['id']."' /></td>";
		} else {
			echo "<td>".$payStatus."</td>";
		}
		echo "</tr>";
	}
	unset($result);

	// Close form
	if ($includeForm) {
		echo "</table><br>";
		echo "<input type='submit' value='Mark as paid' />";
		echo "</form>";
	} else {
		echo "<p>No logs yet for selected period..</p>";
	}
}



