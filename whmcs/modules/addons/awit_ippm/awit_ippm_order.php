<?php
/*
 * AWIT IPPM - Order page
 * Copyright (c) 2013-2014, AllWorldIT
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


// Make sure we not being accssed directly
if (!defined("WHMCS"))
	die("This file cannot be accessed directly");


require_once('awit_ippm_common.php');


// Retrun a hash of product groups indexed with the gid
function awit_ippm_getProductGroups()
{
	$product_groups_res = select_query('tblproductgroups','id,name');
	while ($row = mysql_fetch_assoc($product_groups_res)) {
		$product_groups[$row['id']] = $row['name'];
	}

	return $product_groups;
}


// Retrun a hash of client groups indexed with the gid
function awit_ippm_getClientGroups()
{
	$client_groups_res = select_query('tblclientgroups','id,groupname');
	while ($row = mysql_fetch_assoc($client_groups_res)) {
		$client_groups[$row['id']] = $row['groupname'];
	}

	return $client_groups;
}


// Retrun a hash of custom fields
function awit_ippm_getProductCustomFields($pid)
{
	$where = array( 'relid' => $pid, 'type' => 'product', 'showorder' => 'on' );
	$sort = "sortorder";
	$product_customfields_res = select_query('tblcustomfields','id,relid,fieldtype,fieldname,type,description,fieldoptions,regexpr,required,sortorder',$where,$sort);
	$product_customfields = array();

	$config = awit_ippm_config();

	while ($row = mysql_fetch_assoc($product_customfields_res)) {

		$cfieldname = $AWIT_IPPM_CONFIG_CUSTOM_FIELDS_REVERSE[$row['fieldname']];

		// Do some things to help us out...
		if ($row['fieldtype'] == "dropdown") {
			// Create an array from dropdown field options
			$values = array();
			foreach (explode(',',$row['fieldoptions']) as $item) {
				array_push($values,trim($item));
			}
			$row['fieldoptions'] = $values;
		}

		array_push($product_customfields,$row);
	}

	return $product_customfields;
}


// Return products with group name
function awit_ippm_getProducts()
{
	$product_groups = awit_ippm_getProductGroups();

	$products = localAPI('getproducts')['products']['product'];
	foreach ($products as $id => &$product) {
		$product['group'] = $product_groups[$product['gid']];
		$product['customfields'] = awit_ippm_getProductCustomFields($product['pid']);
	}

	return $products;
}


// Return clients with group name
function awit_ippm_getClients()
{
	$client_groups = awit_ippm_getClientGroups();

	$clients = localAPI('getclients',array( 'limitnum' => 1000 ))['clients']['client'];

	// Build a more pretty client list
	foreach ($clients as $id => &$client) {
		$client['group'] = $client_groups[$client['groupid']];
		if (!empty($client['companyname'])) {
			$client['name'] = $client['companyname']." - ".$client['firstname']." ".$client['lastname'];
		} else {
			$client['name'] = $client['firstname']." ".$client['lastname'];
		}
	}

	return $clients;
}


// Addon output
function awit_ippm_order_output($vars)
{
	global $AWIT_IPPM_CONFIG_CUSTOM_FIELDS;
	global $AWIT_IPPM_CONFIG_CUSTOM_FIELDS_REVERSE;


	if (!empty($_POST['clientid'])) {

echo "<pre>"; print_r($_POST); echo "</pre>";
		// FIXME: Need validation
		$order['clientid'] = $_POST['clientid'];
		$order['paymentmethod'] = $_POST['paymentmethod'];

		$order['pid'] = array();
		$order['billingcycle'] = array();
		$order['domain'] = array();
		$order['customfields'] = array();


		// Figure out what we're doing
		if (empty($_POST['orderconfirmation'])) {
			$order['noemail'] = 1;
		}
		if (empty($_POST['generateinvoice'])) {
			$order['noinvoice'] = 1;
		}
		if (empty($_POST['invoiceemail'])) {
			$order['noinvoiceemail'] = 1;
		}


		foreach ($_POST['product'] as $id => $product) {
			// FIXME: validation
			array_push($order['pid'],$product['id']);
			array_push($order['billingcycle'],$product['billingcycle']);
			array_push($order['domain'],$product['domain']);

			// Loop through custom fields and remove blank ones?
			$customFields = $product['customfields'];
			foreach ($customFields as $name => $value) {
				if (empty($value)) {
					unset($customFields[$name]);
				}
			}
			array_push($order['customfields'],base64_encode(serialize($customFields)));
		}

echo "<pre>"; print_r($order); echo "</pre>";

		$orderRes = localapi('addorder',$order);

echo "<pre>"; print_r($orderRes); echo "</pre>";

		return;
	}

	// Initialize module & grab addon configuration
	awit_ippm_init();
	$config = awit_ippm_config();

	// Grab products for the dropdown list
	$products = awit_ippm_getProducts();
	$productsList = array();
	foreach ($products as $pid => $product) {

		// Loop with product custom fields and add config details if any
		foreach ($product['customfields'] as &$productCustomField) {
			// Check if we have a reverse entry for this custom field
			if (isset($AWIT_IPPM_CONFIG_CUSTOM_FIELDS_REVERSE[$productCustomField['fieldname']])) {
				$internalName = $AWIT_IPPM_CONFIG_CUSTOM_FIELDS_REVERSE[$productCustomField['fieldname']];
				// Check for special types for some custom fields
				if (isset($config['fields'][$internalName]['awitippm_ctype'])) {
					$productCustomField['awitippm_ctype'] = $config['fields'][$internalName]['awitippm_ctype'];
				}
			}
		}

		array_push($productsList,array(
			'category' => $product['group'],
			'label' => $product['name'],
			'data' => (int) $product['pid'],
			'customfields' => $product['customfields']
		));
	}


	$clients = awit_ippm_getClients();
	$clientsList = array();
	foreach ($clients as $pid => $client) {
		array_push($clientsList,array(
			'category' => $client['group'],
			'label' => $client['name'],
			'data' => $client['id']
		));
	}

	$payment_methods = localapi('getpaymentmethods')['paymentmethods']['paymentmethod'];

	function awit_ippm_listsorter($a,$b) {
		return strcasecmp($a['category'].$a['label'],$b['category'].$b['label']);
	}

	usort($clientsList,'awit_ippm_listsorter');
	usort($productsList,'awit_ippm_listsorter');

	// Index products list
	for ($i = 0; $i < count($productsList); $i++) {
		$productsList[$i]['index'] = $i;
	}

//echo "<pre>"; print_r($productsList); echo "</pre>";

?>

		<form method="post" id="awitippm-add-new-order" class="ui-widget">
			<h1>Add New Order</h1>
			<div id="awitippm-client-details">

				<label for="awitippm-clientname">Client</label>
				<input id="awitippm-clientname" type="text" />
				<input class="awitippm-clientid" type="hidden" name="clientid" /> 

				<label for="awitippm-paymentmethod">Payment Method</label>
				<select id="awitippm-paymentmethod" name="paymentmethod">
<?php

					foreach ($payment_methods as $payment_method) {
?>
						<option value="<?php echo htmlspecialchars($payment_method['module']) ?>">
							<?php echo htmlentities($payment_method['displayname']) ?>
						</option>
<?php
					}
?>
				</select>

				<label for="awitippm-promotioncode">Promotion Code</label>
				<input id="awitippm-promotioncode" type="text" name="promotioncode">

				<div class="awitippm-form-item ui-widget">
					<input id="awitippm-orderconfirmation" type="checkbox" name="orderconfirmation" />
					<label for="awitippm-orderconfirmation">Send Order Confirmation</label>

					<input id="awitippm-generateinvoice" type="checkbox" name="generateinvoice" />
					<label for="awitippm-generateinvoice">Generate Invoice</label>

					<input id="awitippm-sendemail" type="checkbox" name="invoiceemail" />
					<label for="awitippm-sendemail">Send Invoice Email</label>
				</div>

				<br/><br/>

				<h2>Products and services</h2>
				<div id="awitippm-products-services">
					<img src="images/icons/add.png" border="0" align="absmiddle" class="awitippm-add-product" />
				</div>

				<br/><br/>

				<h2>Domain Registration</h2>
				<div id="awitippm-domain-registration">
					<img src="images/icons/add.png" border="0" align="absmiddle" class="awitippm-add-domain" />
				</div>
			</div>

			<input type="submit" value="Submit Order »" class="btn-primary" />

		</form>

<script>
var product_count = 0;
var domain_count = 0;
var productsList = <?php echo json_encode($productsList) ?>;
var clientsList = <?php echo json_encode($clientsList) ?>;


$(document).ready(function(e) {
	// init add product
	awitippm_productline_add();
	awitippm_domainline_add();


	$(function() {
		$('#awitippm-clientname').catcomplete({
			delay: 0,
			source: clientsList,
			select: function( event, ui ) {
				$(this).val( ui.item.label );

				var idinput = $(this).parent().find('.awitippm-clientid');
				idinput.val(ui.item.data);

				return false;
			}
		});
	});

	// toggle function
	$(document).on('click', '.awitippm-toggle',
		function() {
			var parent = $(this).parent().next();

			if (parent.hasClass('awitippm-min')) {
				parent.removeClass('awitippm-min').addClass('awitippm-max');
			} else {
				parent.removeClass('awitippm-max').addClass('awitippm-min');
			};
		}
	);

	// Add product
	$(document).on( 'click', '.awitippm-add-product',
		function() {
			awitippm_productline_add();
			$('.awitippm-first .awitippm-delete-product').show();
		}
	);
	// Add domain
	$(document).on( 'click', '.awitippm-add-domain',
		function() {
			awitippm_domainline_add();
			$('.awitippm-first .awitippm-delete-domain').show();
		}
	);

	// Delete product
	$(document).on( 'click', '.awitippm-delete-product',
		function() {
			awitippm_productline_remove($(this))
		}
	);
	// Delete domain
	$(document).on( 'click', '.awitippm-delete-domain',
		function() {
			awitippm_domainline_remove($(this));
		}
	);

});
</script>


<?php
}



// vim: ts=4
