/*
 * AWIT IPPM - Javascript functions
 * Copyright (c) 2013-2014, AllWorldIT
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */



// Add product function
function awitippm_productline_add() {

	var product_num = product_count;

	// products template ************* edit this ***********
	var content = ' \
		<div class="awitippm-product" id="product-line-'+product_num+'"> \
			<div class="awitippm-product-main-line"> \
				<input class="awitippm-product-name" placeholder="Product/Services" type="text" /> \
				<input class="awitippm-product-id" type="hidden" name="product['+product_num+'][id]" /> \
\
				<input class="awitippm-domain" placeholder="Domain" type="text" name="product['+product_num+'][domain]" /> \
				<select class="awitippm-billing-cycle" placeholder="Billing Cycle" name="product['+product_num+'][billingcycle]"> \
					<option value="Monthly" selected>Monthly</option> \
					<option value="Quarterly">Quarterly</option> \
					<option value="Semi-Annually">Semi-Annually</option> \
					<option value="Annually">Annually</option> \
					<option value="Biennially">Biennially</option> \
					<option value="Triennially">Triennially</option> \
					<option value="Free Account">Free</option> \
					<option value="One Time">One Time</option> \
				</select> \
				<input class="awitippm-qty" placeholder="Qty" type="text" name="product['+product_num+'][qty]" /> \
				<input class="awitippm-price" placeholder="Price" type=s"text" name="product['+product_num+'][price]" /> \
				<img src="images/icons/tickets.png" width="16" height="16" border="0" alt="Details" class="awitippm-toggle" /> \
				<img src="images/delete.gif" width="16" height="16" border="0" alt="Delete" class="awitippm-delete-product" /> \
			</div> \
			<div class="awitippm-product-extra-line"></div> \
		</div> \
	';

	// Add product to block
	$('#awitippm-products-services').append(content);

	// Grab product items, we only need to ever do this once
	var product = $('#product-line-'+product_num);
	var product_name = product.find('.awitippm-product-name');
	var product_id = product.find('.awitippm-product-id');
	var product_options = product.find('.awitippm-product-extra-line');

	// Attach everything we need
	product_name.catcomplete({
		delay: 0,
		source: productsList,
		select: function(event, ui) {
			// this == product_name
			$(this).val(ui.item.label);

			// Set the id fields value
			product_id.val(ui.item.data);

			// Empty the options list
			product_options.empty();

			// Loop with custom fields to add
			for (var fieldIDX = 0; fieldIDX < ui.item.customfields.length; fieldIDX++) {
				var field = ui.item.customfields[fieldIDX];

				var elementID = 'awitippm-option'+field.id;

				// Decide how we going to treat the types
				if (field.fieldtype === "text") {

					product_options.append(' \
						<div class="awitippm-half"> \
							<label for="'+elementID+'">'+awit_ippm_escape_html(field.fieldname)+'</label> \
							<input class="awitippm-option-'+field.awitippm_ctype+'" id="'+elementID+'" type="text" \
									name="product['+product_num+'][customfields]['+field.id+']" /> \
						</div> \
					');

					// If its a date
					if (field.awitippm_ctype === "date") {
						var dateoption = product_options.find('#'+elementID);

						// Attach a date picker
					   	dateoption.datepicker({
							numberOfMonths: 3,
							showButtonPanel: true,
							dateFormat: 'yy-mm-dd'
						});

					}

				} else if (field.fieldtype === "dropdown") {
					var fieldOptions = field.fieldoptions;

					// Build select box
					var optionLine = ' \
						<div class="awitippm-half"> \
							<label for="'+elementID+'">'+awit_ippm_escape_html(field.fieldname)+'</label> \
							<select class="awitippm-option-'+field.awitippm_ctype+'" id="'+elementID+'" \
									name="product['+product_num+'][customfields]['+field.id+']"> \
					';
					for (var optionIDX = 0; optionIDX < fieldOptions.length; optionIDX++) {
						optionLine = optionLine + ' \
								<option value="'+awit_ippm_escape_html(fieldOptions[optionIDX])+'"> \
									'+awit_ippm_escape_html(fieldOptions[optionIDX])+' \
								</option> \
						';
					}
					optionLine = optionLine + ' \
							</select> \
						</div> \
					';

					product_options.append(optionLine);

				}

			}

			return false;
		}
	});


	return product_count++;
}


/*
				<div class="awitippm-form-item awitippm-half"> \
					<label for="extra-1">Extra-1</label> <input name="extra-1" placeholder="extra-1" type="text" /> \
				</div> \
				<div class="awitippm-form-item awitippm-half"> \
					<label for="extra-1">Extra-1</label> <input name="extra-1" placeholder="extra-1" type="text" /> \
				</div> \
				<div class="awitippm-form-item awitippm-half"> \
					<label for="extra-1">Extra-1</label> <input name="extra-1" placeholder="extra-1" type="text" /> \
				</div> \
*/

function awitippm_domainline_add() {
	var first = '';

	if (domain_count == 0) {
		first = ' hidden';
	}

	var domain_product = ' \
		<div class="awitippm-domain-registration" id="domain-registration-product-'+domain_count+'"> \
			<img src="images/delete.gif" width="16" height="16" border="0" alt="Delete" class="awitippm-delete-domain" /> \
		<div class="awitippm-form-item"> \
			<input id="domnon0" type="radio" checked="" onclick="domainoptions(this,0);" value="" name="regaction[0]" /> \
			<label for="domnon0">None</label> \
			<input id="domreg0" type="radio" onclick="domainoptions(this,1);" value="register" name="regaction[0]" /> \
			<label for="domreg0">Registration</label> \
			<input id="domtrf0" type="radio" onclick="domainoptions(this,2);" value="transfer" name="regaction[0]" /> \
			<label for="domtrf0">Transfer</label> \
		</div> \
	';

	$('#awitippm-domain-registration').append(domain_product);

	return domain_count++;
}


// Delete product
function awitippm_productline_remove(el) {
	var line = el.parent().parent();

	var sure = confirm('Are you sure?');

	if (sure) {
		line.remove();
	} else {
		return false;
	}
};


// Delete domain
function awitippm_domainline_remove(el) {
	var line = el.parent();

	var sure = confirm('Are you sure?');

	if (sure) {
		line.remove();
	} else {
		return false;
	}
};


// populate Domain options
function domainoptions(el,type){
	var domain_defaults	= ' \
		<div class="optionsDomain"> \
			<div class="awitippm-form-item"> \
				Domain \
				<input type="text" id="regdomain0" size="40" name="regdomain[0]" class="regdomain" /> \
			</div> \
			<div class="awitippm-form-item"> \
				Registration Period \
				<select name="regperiod[0]" id="regperiod1"> \
					<option value="1">1 Year</option> \
					<option value="2">2 Years</option> \
					<option value="3">3 Years</option> \
					<option value="4">4 Years</option> \
					<option value="5">5 Years</option> \
 					<option value="6">6 Years</option> \
					<option value="7">7 Years</option> \
					<option value="8">8 Years</option> \
					<option value="9">9 Years</option> \
					<option value="10">10 Years</option> \
				</select> \
			</div> \
	';

	if (type == 2) {
		domain_defaults += ' \
			<div class="awitippm-form-item"> \
				EPP code \
				<input type="text" size="20" name="eppcode[0]" /> \
			</div> \
		';
	}

	domain_defaults += ' \
			<label> \
				<input type="checkbox" name="dnsmanagement[0]" /> \
				DNS Management \
			</label> \
			<label> \
				<input type="checkbox" name="emailforwarding[0]" /> \
				Email Forwarding \
			</label> \
			<label> \
				<input type="checkbox" onclick="updatesummary()" name="idprotection[0]"> \
				ID Protection</label> \
		</div> \
	';

	if (type == 0) {
		domain_defaults='';
	}

	$(el).parent().parent().find('.optionsDomain').remove()
	$(el).parent().parent().append(domain_defaults);
}


// Setup auto complete
$.widget('custom.catcomplete', $.ui.autocomplete, {
	_renderMenu: function( ul, items ) {
		var that = this,currentCategory = "";
		$.each( items, function( index, item ) {
			if ( item.category != currentCategory ) {
				ul.append( "<li class='awitippm-autocomplete-category'>" + item.category + "</li>" );
				currentCategory = item.category;
			}
			that._renderItemData( ul, item );
		});
	}
});

// Fucntion to escape html
awit_ippm_escape_html = function(s) {
	return String(s).replace(/&(?!\w+;)/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}

