<?php
/*
 * AWIT IPPM - Invoice Post Processing Module
 * Copyright (c) 2013-2016, AllWorldIT
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


// Make sure we not being accssed directly
if (!defined("WHMCS"))
	die("This file cannot be accessed directly");



require_once('awit_ippm_config.php');



// Addon configuration
function awit_ippm_config() {
	return _awit_ippm_config();
}



// Addon activation
function awit_ippm_activate() {

	// Create Custom DB Table
	$result = mysql_query("
		CREATE TABLE `mod_awit_ippm` (
			`id` INT( 1 ) NOT NULL AUTO_INCREMENT PRIMARY KEY,
			`timestamp` DATETIME NOT NULL,
			`invoice_id` VARCHAR(10) NOT NULL,
			`sales_person` VARCHAR(30) NOT NULL,
			`sales_commission` DECIMAL(8,2) NOT NULL,
			`sales_commission_s` VARCHAR(80) NOT NULL,
			`paid` SMALLINT(1) NOT NULL DEFAULT 0
		)
	");

	// Return Result
	if (!$result) {
		return array("status" => "error", "description"=>"There was a problem activating the module.");
	} else {
		return array("status" => "success","description" =>"Open module configuration for configuration options.");
	}

}



// Deactivate the addon
function awit_ippm_deactivate() {

	// Remove Custom DB Table
	$result = mysql_query("
		DROP TABLE `mod_awit_ippm`
	");

	// Return Result
	if (!$result) {
		return array("status"=>"error","description"=>"There was an error deactivating the module.");
	} else {
		return array("status"=>"success","description"=>"Module has been deactivated.");
	}

}



// Upgrade the addon
function awit_ippm_upgrade($vars) {

	$version = $vars['version'];

	// Run SQL Updates for V1.0 to V1.1
	//if ($version < 1.3) {
	//}

}



/*
 * WEB INTERFACE - Order Page
 */


// Addon output
function awit_ippm_output($vars) {
	require_once('awit_ippm_order.php');

	return awit_ippm_order_output($vars);
}


