<?php
/*
 * AWIT IPPM - WHMCS interface class for test suite
 * Copyright (c) 2013-2014, AllWorldIT
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


// Set include path to WHMCS
set_include_path(get_include_path() . PATH_SEPARATOR . "/var/www/whmcs");
// Pull in WHMCS
require_once("init.php");



class AWITWHMCS
{
	private $admin_user;


	// Instantiate the object and set admin_user
	public function __construct($admin_user)
	{
		$this->admin_user = $admin_user;
	}


	// api_addClient
	public function api_addClient($values)
	{
		$res = localapi('addclient',$values,$this->admin_user);
		if ($res['result'] === "error") {
			print_r($res);
			return NULL;
		}
		return $res['clientid'];
	}


	// api_getClients
	public function api_getClients()
	{
		$res = localapi('getclients',array( 'limitnum' => 999 ),$this->admin_user);
		if ($res['result'] === "error") {
			print_r($res);
			return NULL;
		}
		return $res['clients']['client'];
	}


	// api_deleteClient
	public function api_deleteClient($clientid)
	{
		$res = localapi('deleteclient',array( 'clientid' => $clientid ),$this->admin_user);
		if ($res['result'] === "error") {
			print_r($res);
			return NULL;
		}
		return TRUE;
	}


	// custom_addProductGroup
	public function custom_addProductGroup($values)
	{
		$res = insert_query("tblproductgroups",$values);
		return $res;
	}


	// custom_deleteProductGroup
	public function custom_deleteProductGroup($gid)
	{
		delete_query("tblproductgroups",array( 'id' => $gid ));
	}


	// api_addProduct
	public function api_addProduct($values)
	{
		$res = localapi('addproduct',$values,$this->admin_user);
		if ($res['result'] === "error") {
			print_r($res);
			return NULL;
		}
		return $res['pid'];
	}


	// api_deleteProduct
	public function api_deleteProduct($pid)
	{
		delete_query("tblpricing", array('type' => "product", 'relid' => $pid));
    		delete_query("tblcustomfields", array('type' => "product", "relid" => $pid));
    		full_query("DELETE FROM tblcustomfieldsvalues WHERE fieldid NOT IN (SELECT id FROM tblcustomfields)");
    		delete_query("tblproductconfiglinks", array('pid' => $pid));
		delete_query("tblproducts", array( 'id' => $pid) );
	}


	// custom_addProductCustomField
	public function custom_addProductCustomField($pid,$values)
	{
		$values['type'] = "product";
		$values['relid'] = $pid;
		$res = insert_query("tblcustomfields",$values);
		return $res;
	}


	// api_addOrder
	public function api_addOrder($values)
	{
		$values['noemail'] = 1;
		$res = localapi('addorder',$values,$this->admin_user);
		if ($res['result'] === "error") {
			print_r($res);
			return NULL;
		}
		return $res['orderid'];
	}


	// api_getInvoice
	public function api_getInvoice($invoiceid)
	{
		$res = localAPI('getinvoice',array('invoiceid' => $invoiceid),$this->admin_user);
		if ($res['result'] === "error") {
			print_r($res);
			return NULL;
		}

		return $res;
	}


	// custom_getInvoiceIDFromOrderID
	public function custom_getInvoiceIDFromOrderID($orderid)
	{
		$result = select_query('tblorders','invoiceid',array( 'id' => $orderid ));
		$row = mysql_fetch_assoc($result);
		if (!isset($row)) {
			return NULL;
		}
		return $row['invoiceid'];
	}


	// api_getClientsProducts
	public function api_getClientsProducts($clientid)
	{
		$res = localAPI('getclientsproducts',array('clientid' => $clientid),$this->admin_user);
		if ($res['result'] === "error") {
			print_r($res);
			return NULL;
		}
		$results = array();
		foreach ($res['products']['product'] as $id => $product) {
			$results[$product['id']] = $product;
		}

		return $results;
	}


	// custom_enablePaymentMethod
	public function custom_enablePaymentMethod($method)
	{
/*
mysql> select * from tblpaymentgateways;
+---------+---------+-----------------+-------+
| gateway | setting | value           | order |
+---------+---------+-----------------+-------+
| mailin  | name    | Mail In Payment |     1 |
| mailin  | type    | Invoices        |     0 |
| mailin  | visible | on              |     0 |
+---------+---------+-----------------+-------+
3 rows in set (0.00 sec)
*/
	}


	// custom_getGatewayStatus
	public function custom_getGatewayStatus($gateway)
	{
		$result = select_query('tblpaymentgateways','value',array( 'gateway' => $gateway, 'setting' => 'name' ));
		$row = mysql_fetch_assoc($result);
		if (!isset($row)) {
			return NULL;
		}
		return isset($row);
	}


	// custom_getAddonConfig
	public function custom_getAddonConfig($module,$setting)
	{
		$result = select_query('tbladdonmodules','value',array( 'module' => $module, 'setting' => $setting ));
		$row = mysql_fetch_assoc($result);
		if (!isset($row)) {
			return NULL;
		}
		return $row['value'];
	}


	// custom_getConfig
	public function custom_getConfig($setting)
	{
		$result = select_query('tblconfiguration','value',array( 'setting' => $setting ));
		$row = mysql_fetch_assoc($result);
		if (!isset($row)) {
			return NULL;
		}
		return $row['value'];
	}


	// custom_getConfigArray
	public function custom_getConfigArray($setting)
	{
		$result = select_query('tblconfiguration','value',array( 'setting' => $setting ));
		$row = mysql_fetch_assoc($result);
		if (!isset($row)) {
			return NULL;
		}
		return explode(',',$row['value']);
	}


	// custom_getAdminUser
	public function custom_getAdminUser($user)
	{
		$result = select_query('tblconfiguration','id',array( 'username' => $admin ));
		$row = mysql_fetch_assoc($result);
		if (!isset($row)) {
			return NULL;
		}
		return $row['id'];
	}


	// custom_getConfigArrayItemExists
	public function custom_getConfigArrayItemExists($setting,$item)
	{
		return in_array($item,$this->custom_getConfigArray($setting));
	}


	// custom_setConfig
	public function custom_setConfig($setting,$value)
	{
		$result = update_query('tblconfiguration',array('value' => $value),array( 'setting' => $setting ));
	}


	// custom_setConfigArray
	public function custom_setConfigArray($setting,$values)
	{
		$result = update_query('tblconfiguration',array('value' => implode(',',sort($values))),array( 'setting' => $setting ));
	}



}

